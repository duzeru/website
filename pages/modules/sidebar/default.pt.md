---
shortcode-citation:
    items: cited
    reorder_uncited: true
routable: false
---

#### Parceiros

[DioLinux](https://diolinux.com.br)

[SempreUpdate](https://sempreupdate.com.br)
