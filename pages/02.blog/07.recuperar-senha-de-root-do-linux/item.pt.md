---
title: 'Recuperar senha de root do Linux'
media_order: '001.png,002.png,003.png,004.png,005.png,006.png,007.png,reset-senha.mp4'
shortcode-citation:
    items: cited
    reorder_uncited: true
feed:
    limit: 10
---

## Sumário 
 - [ Objetivo ](#Objetivo) 
 - [ Abrangência ](#Abrangencia) 
 - [ Conceitos e Definições ](#Conceitos) 
 - [ Fluxo de atividades ](#Atividades) 
 - [ Referências ](#Referencias) 

#### Objetivo: <a name='Objetivo'></a> 
Existem algumas maneiras de se recuperar a senha do usuário **administrador** _(ou do super usuário)_ no Linux. Uma muito comum é alterar o modo que o sistema inicia, ou seja, quando realiza o boot. Dessa forma, acessamos o sistema como superusuário e alterar a senha.

#### Abrangência: <a name='Abrangencia'></a> 
Este procedimento foi testado em distribuições como:
* Debian
* Ubuntu
* CentOS
* Oracle

#### Conceitos e Definicões: <a name='Conceitos'></a> 
Precisa-se antes de mais nada, entender como funciona o boot no sistema e como configurar o boot loader para carregar o sistema como root. 
[notice]Para isso, precisamos antes entender melhor o que seria o boot![/notice]

##### Entendendo o boot
Boot nada mais é do que o momento em que sua máquina está sendo ligada. Nesse momento, um programa chamado BIOS carrega algumas informações sobre o hardware do computador e o checa. Após esse processo ela chama o gerenciador de boot (boot loader) que carrega o sistema operacional.
Existem diversos gerenciadores disponíveis. No caso do Linux, esse gerenciador mais comum é o **GRUB**, este é o boot loade utilizado neste procedimento.

### Fluxo de atividades: <a name='Atividades'></a> 

#### Vídeo apresentando o procedimento na prática:
![](reset-senha.mp4)

#### 1. Acessando o GRUB
Conseguimos acessar o GRUB no momento em que a máquina está ligando. Basta apertar a tecla Esc, ou Shift. Após um tempo, uma tela parecida com está deve aparecer:

![](001.png)

Queremos falar para o GRUB que desejamos acessar o sistema como usuário administrador, dessa forma conseguimos modificar a senha.

**1.1 Editar linha do GRUB**
Para efetuar a alteração _(temporária)_ no GRUB, temos que editar uma linha em sua configuração. Logo, pressionamos **e** (edit) para editar as informações. Após pressionar **e**, o GRUB apresentará um arquivo com  informações (parâmetros) para o kernel, isto é, o núcleo do sistema operacional. Algumas dessas informações são: o sistemas de arquivos do root, o tipo de montagem de uma partição, entre outros.
A linha principal que devemos editar geralmente inicia-se com a palavra **linux /boot/vmlinuz...**

![](002.png)

Queremos entrar como super usuário no momento em que o Linux é carregado. Precisamos dizer para o GRUB iniciar um terminal assim que o sistema carregar, dessa forma conseguiremos realizar as alterações. Para isso é necessário adicionar o parâmetro abaixo no final da linha.
```
init=/bin/bash
```

Resultado:
![](003.png)

Pronto! a etapa 1.1 está concluída e as configurações realizadas! deve ser agora pressionado para o sistema iniciar com essas configurações a combinação de teclas **Ctrl + x** ou simplesmente **F10**.

**1.2 Alterar a senha**
Após carregar o sistema efetuando a etapa anterior, o sistema é carregado com o sistema de arquivos **somente leitura**, caso você tentar alterar a senha ocasionará erro.

![](004.png)

Para efetuar a alteração de senha, temos que remontar o sistema de arquivos com leitura e escrita da seguinte forma:
```
mount -o remount,rw /
```

Não aparecerá absolutamente nenhuma mensagem ao executar a linha de comando, mas, agora pode ser efetuado a alteração da senha:

![](005.png)


Deve ser reiniciado o servidor para ser validado sua troca de senh.

!! Quando estamos como monousuário no GRUB, não conseguimos reiniciar o computador com esses comando como reboot.
!! Então, como podemos reiniciar nosso computador?

Execute o comando 
```
/sbin/init 6
```
![](006.png)

Seu servidor será reiniciado, caso não reiniciar com esta linha de comando, deve ser efetuado de forma forçada. Após reiniciar, seu sistema aceitará a nova senha.

![](007.png)

#### Referências: <a name='Referencias'></a> 
 - [https://www.alura.com.br/artigos/como-recuperar-senha-de-root-no-linux](https://www.alura.com.br/artigos/como-recuperar-senha-de-root-no-linux)
 - [https://t.me.duzeru](https://t.me.duzeru)