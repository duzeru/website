---
title: '9 Tendências e inovações em Open Source'
media_order: 'open-source.png,clicka.PNG'
shortcode-citation:
    items: cited
    reorder_uncited: true
feed:
    limit: 10
---

A tecnologia Open Source é uma das mais flexíveis, escaláveis e inovadoras do mercado
de TI. Isso se dá por tratar-se de uma comunidade pluralista e colaborativa, que juntam
suas habilidades e valores para desenvolverem diversos projetos.
Por isso, existem diversas tendências e inovações surgindo atualmente no meio Open
Source. Abaixo, reunimos as principais que você deveria se atentar, algumas das quais
Chris Ferris, CTO de Tecnologias Abertas da IBM, apontou como importantes. Confira!

**1. Empreendimento Open Source devem investir em UX**
É comum que empresas de código aberto direcionem os seus esforços para competir com
provedores de computação em nuvem. Uma tendência entre esses esforços é investir mais
fortemente na experiência do usuário. O Customer Experience (CX) vem se tornando cada
vez mais comum no mercado por mostrar-se eficiente na fidelização de clientes e
estratégico diante à concorrência.

**2. Softwares deverão se adequar aos usos de negócios**
Uma das grandes vantagens do open source é a flexibilidade e compatibilidade. Por isso,
uma das tendências é que cada vez mais negócios adotem softwares de códigos abertos
em detrimento de plataformas inflexíveis. Dessa forma, empresas não terão de se adaptar à
softwares pois terão a possibilidade de escolher o sistema operacional que melhor contribua
para seu trabalho, além de se integrar ao mesmo.

**3. Softwares serão encorajados a adotar uma camada fundamental de código aberto**
O código aberto será cada vez mais fundamental em aplicativos de software. Isso porque os
empreendimentos estarão mais propensos em investir em uma camada de experiência do
usuário e em outra camada de base open source colaborativa e livremente modificável. O
motivo desse incentivo é o cenário de parceria e cooperação que vem se alastrando em
todos os meios. Por isso, empresas estão cada vez mais encorajadas a utilizar modelos de
colaboração visando um proveito comercial mais vantajoso.

**4. Maior controle sobre os dados com Open Source Open**
Em um cenário conturbado de privacidade de dados como o atual, tecnologias open source
estão se tornando uma tendência. Ao tratar-se de plataformas de código aberto, os usuários
possuem maior controle sobre os seus dados e melhor conhecimento de como eles estão
sendo usados. Além disso, a preocupação com o consentimento de uso dos dados é
reduzida pois, por ser uma tecnologia baseada em nuvem, todas as informações
compartilhadas são concedidas pelos próprios consumidores.

**5. Containers e micro serviços menores e mais rápidos**
Projetos de open source como Istio, Kubernetes e OKD podem abrir caminho para serviços
e containers menores e mais rápidos. Esse foco pode vir para suprir necessidades de
desenvolvimento nativo na nuvem e diminuir ataques em containers. Além disso, unikernels
(imagens executáveis que têm bibliotecas de sistema) também podem vir a ser cada vez
mais populares devido aos grupos de open source que os cercam.

**6. Cargas de trabalho serverless instantâneas**
Os desafios de tornar plataformas serverless mais rápidas continuará a ser uma inovação
cada vez maior. O grande objetivo é tornar as cargas de trabalho serverless instantâneas, e
com as inovações de projetos de código aberto, acredita-se que essa meta poderá ser
cumprida em breve.

**7. Novos usos para as capacidades de rastreamento de blockchain**
A tecnologia de blockchain foi muito útil e amplamente utilizada nas criptomoedas. Porém,
essa é uma inovação que ainda pode ser imensamente explorada, e essa é a tendência
para os próximos anos. Inovações feitas como código abertos estão abrindo caminho para
que as capacidades de rastreamento de blockchain tenham novos usos, como por exemplo
no gerenciamento de identidade, nas cadeias de suprimento, na gestão de ativos, entre
outros.

**8. Processadores quânticos disponíveis para desenvolvedores**
A computação quântica é uma promessa empolgante que poderá revolucionar diversas
áreas como química, inteligência artificial, finanças, entre outros. Apesar de ainda não ser
uma realidade, a tendência é se preparar cada vez mais para capacitar desenvolvedores à
utilizar processadores quânticos.
Um exemplo de como essa inovação já está dando os primeiros pontapés é o software de
código aberto da IBM, o Qiskit, que permite desenvolvedores a programar em Python
utilizando hardware quântico real.

**9. Inteligência Artificial confiável**
A Inteligência Artificial é uma inovação já amplamente utilizada e que vem avançando cada
vez mais. Porém, atualmente viu-se a necessidade não só de melhorar as tecnologias em
volta do IA mas também de tornar mais confiável os sistemas inteligentes. O código aberto
pode ser um caminho para aumentar a confiabilidade da Inteligência Artificial , como já
mostraram os projetos Adversarial Robustness 360 Toolkit, AI Fairness 360 Open Source
Toolkit e AI Explainability 360 Toolkit.
Curtiu as dicas sobre inovação e tendências? É sempre importante ficar atento às
mudanças do mercado, pois há muitas [vagas de TI](https://clickaonline.com/) disponíveis a quem está atualizado
sobre o seu próprio meio.
Esperamos que você tenha gostado do conteúdo!
Até a próxima, [ClickA Online](https://clickaonline.com/) .

[![](clicka.PNG)](https://clickaonline.com)

![](open-source.png)