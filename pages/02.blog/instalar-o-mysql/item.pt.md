---
title: 'Como instalar o MySQL no DuZeru, Debian, ubuntu, mint e derivados.'
media_order: 'mysql_01-720x383.jpg,0001.png,mysql-logos.gif'
shortcode-citation:
    items: cited
    reorder_uncited: true
visible: true
feed:
    limit: 10
---

![](mysql-logos.gif)

O MySQL é muito provavelmente o sistema de gestão de base de dados (SGBD) mais usado em todo o mundo estando disponível em várias versões e para diferentes sistemas operativos.

Tudo o que é plataformas open source como é o caso do wordpress, Joomla ou Drupal, recorrem ao MySQL como base de dados. Para instalar o MySQL no Debian devem seguir os seguintes passos:

#### Passo 1 – Configurar PPA MySQL
```
wget http://repo.mysql.com/mysql-apt-config_0.8.13-1_all.deb
sudo dpkg -i mysql-apt-config_0.8.13-1_all.deb
```

Em seguida procedam à configuração do MySQL, podem escolher qual a versão do MySQL que pretendem. Para este tutorial vamos escolher a versão 5.7.
![](mysql_01-720x383.jpg)

#### passo 3 –  Instalar o MySQL
Para procederem à instalação do MySQL basta que usem o seguinte comando:
```
sudo apt-get update
sudo apt install mysql-server
```

Passo 4 –  Aceder ao MySQL
O acesso ao MySQL é feito com o seguinte comando:
```
mysql -u root -p
```

E está feito! Instalar o MySQL em qualquer distribuição Linux é muito simples.

#### passo 4 – Manipulando o serviço

para manipular o serviço para verificar o status, parar, subir novamente execute:

```
systemctl status mysql
systemctl start mysql
systemctl stop mysql
```
![](0001.png)

A Oracle Corporation detém atualmente o MySQL apesar de existir um fork do projeto com o nome MariaDB. O MySQL compete com projetos como o SQL Server da Microsoft ou como o PostgreSQL e usa a linguagem SQL (Structured Query Language) para a realização de todas as operações (ex. criação de base de dados, inserção e consulta de dados, etc).

Fonte: [https://pplware.sapo.pt/linux/debian-10-buster-aprenda-a-instalar-o-mysql/](https://pplware.sapo.pt/linux/debian-10-buster-aprenda-a-instalar-o-mysql/)