---
title: 'WallPaper e ícones desaparecidos na área de trabalho Xfce- Problema com o xfdesktop'
media_order: duzeru-tela-cinza.jpeg
shortcode-citation:
    items: cited
    reorder_uncited: true
feed:
    limit: 10
---

Bem, acho que quem utiliza o Xfce já se pegou pelo menos uma vez com esse erro. Agora tenho certeza absoluta que você que está lendo está passando por isso.  
Que bom que eu tenho a solução para você.

**VAMOS A CAUSA!**  
![](https://duzeru.org/user/pages/02.blog/como-utilizar-o-comando-awk-no-linux/magic2.gif)

O problema é que o xfdesktop está morto, não há papel de parede nem ícones na sua área de trabalho, apenas uma cor geralmente cinza, os ícone na área de trabalho e o papel de parede simplesmente sumiram.
Não sei dizer ao certo, em uma pequena pesquisa no Fórum do Xfce encontrei o [Bug # 9892](https://bugzilla.xfce.org/show_bug.cgi?id=9892) que explica melhor o problema. A solução está neste [commit do git do Xfce](https://git.xfce.org/xfce/xfdesktop/commit/?id=8672ff1f791a9801b9309052d053f8b12c4adab6).
Mas tenho certeza que algo aconteceu na sua sessão anterior como uma atualização deste próprio xfcedesktop ou outro que  o impactou.

**SOLUÇÃO SIMPLES**
Execute os seguintes comandos:
```
rm -R ~/.cache/sessions ; xfdesktop
 ```
 
 ![](https://duzeru.org/user/pages/02.blog/como-utilizar-o-comando-awk-no-linux/magic.gif)
  
 Atenção o próximo comando vai reiniciar seu Sistema Operacional:  

 ```
 xfce4-session-logout --reboot
 ```
 
  Caso você reinicie seu sistema operacional com o comando acima e não voltar a sua área de trabalho, reexecute o comando
  ```
  xfdesktop
  ```

Visite o nosso [grupo no Telegram](https://t.me/duzeru)

Fonte: [https://forum.xfce.org/viewtopic.php?id=7793](https://forum.xfce.org/viewtopic.php?id=7793)