---
title: 'Os mundos perdidos do Telnet'
media_order: 'telnet 00.png,telnet.png,telnet2.png,jon-postel.jpg'
shortcode-citation:
    items: cited
    reorder_uncited: true
feed:
    limit: 10
content:
    items: '- ''@self.children'''
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: '1'
---

 **Que a linha de comando viva para sempre**

A maioria das pessoas pensa no Telnet como "o que eu costumava usar para acessar servidores remotamente". Mas algumas almas saudáveis ainda mantêm seus serviços Telnet on-line - e é uma ótima maneira de experimentar uma boa diversão antiquada que desperdiça tempo!

Embora, como ferramenta de trabalho, o Telnet tenha sido descontinuado em favor do Secure Shell (SSH), alguns minutos de exploração revelam rapidamente que ainda existe uma subcultura esquecida nos locais em que o Telnet pode levá-lo. "Conecte-se a outros servidores através do Telnet para visualizar suas artes, jogos ASCII animados etc.", explicou um diretório no [Mewbies](http://mewbies.com/), um site que oferece tutoriais sobre "a instalação e uso de softwares (principalmente não convencionais)". Última atualização em 2014, o site inclui uma lista intitulada " [DIVERTIMENTO NO TERMINAL](http://mewbies.com/acute_terminal_fun_telnet_public_servers_watch_star_wars_play_games_etc.htm) " juntamente com algumas instruções simples. Se você ainda não está acessando o Telnet na linha de comando da sua conta shell, basta colar o endereço Telnet do site em qualquer cliente de terminal.

#### Retro Fun

O serviço Telnet mais famoso é provavelmente a versão artística ASCII do "Star Wars" original - sim, o filme inteiro - que é exibido com toda a sua glória quando você Telnet usa o toalha.blinkenlights.nl ("toalha telnet.blinkenlights.nl") . Agora também é cuidadosamente arquivado e visível através de um navegador da Web em [Asciimation.co.nz](http://www.asciimation.co.nz/) . “Se você tem um IPv6, pode assistir em cenas coloridas e extras”, aponta Mewbies.

![](telnet2.png)

Mas esperar em uma porta diferente no mesmo servidor é algo que fará com que todo administrador de sistemas sorria: um servidor BOFH Excuse. O "Bastard Operator from Hell" é um personagem lendário do [folclore nerd](https://thenewstack.io/sys-admin-appreciation-day-bofh/) que usa seu conhecimento tecnológico superior para eliminar suas frustrações em usuários sem noção. Entre em **Telnet para towel.blinkenlights.nl 666** e você receberá uma **tagarelice**

Um dos sites de telnet mais notáveis é o [telehack.com](http://www.telehack.com/) (que também está disponível no seu navegador ).
Lá, um usuário chamado Forbin "decidiu recriar o máximo possível do ARPANet para as pessoas explorarem", de acordo com um perfil de 2011 no [TechRadar](https://www.techradar.com/news/networking/hack-virtual-1980s-networks-with-telehack-1035135) .
Suas ofertas incluem "advento", a aventura de texto original de 1976 sobre a exploração da Colossal Cave, bem como o clássico "chatbot eliza" de 1966, que tentou simular uma conversa com um psicoterapeuta. Existem até [leitores da Usenet](https://www.techopedia.com/definition/3210/usenet) que realmente postam posts dos fóruns de discussão da Usenet de 1990. Quando acessei o Telehack.com na quinta-feira, sua tela de boas-vindas incluía esta saudação reconfortante: "**Que a linha de comando viva para sempre**".

"Telehack é um tipo de jogo e há níveis a serem alcançados", [explicou o TechRadar](https://www.techradar.com/news/networking/hack-virtual-1980s-networks-with-telehack-1035135/2) . “Quando você aprender uma nova habilidade de hackers, uma mensagem do sistema de uma conta chamada Operator informará seu progresso. Você também terá acesso a mais comandos e recursos do sistema. ” Se você finalmente executar o comando **netstat** , verá uma lista de mais hosts simulados nos quais pode fazer login. Há também uma maneira de verificar hosts remotos em busca de portas exploráveis, nas quais você pode executar rootkits para obter acesso total no nível de administrador. Existe até uma simulação de um “guardião” da velha escola, que telefonava para todos os números em um determinado código de área procurando modems dial-up.

#### As origens do Telnet

No IETF.org, você ainda pode encontrar o "[pedido de comentários](https://tools.ietf.org/html/rfc318)" original de 1972 sobre o protocolo Telnet. Foi escrito por [Jon Postel](https://internethalloffame.org/inductees/jon-postel) , uma figura importante nos primeiros dias do desenvolvimento do padrão da Internet, que esteve envolvido no projeto ARPANET enquanto ainda trabalhava em seu Ph.D.

![](jon-postel.jpg)

Mas o protocolo parece ter tido um início ignominioso. “Na reunião do grupo de trabalho em rede de outubro de 1971”, escreveu Postel, “prometi produzir imediatamente um documento que especificasse clara e sucintamente e explicasse o Protocolo Telnet Oficial. Este documento falha em cumprir qualquer parte dessa promessa.

“Este documento não foi produzido prontamente. Este documento não é claro nem sucinto. Não existe protocolo oficial de Telnet. ”

Ele continua explicando que, em vez disso, a Telnet tinha um protocolo ad hoc e seu objetivo final era fornecer uma espécie de teletipo virtual.

Era uma ferramenta usada com frequência nos primeiros dias da Internet - pelo menos antes que a maioria dos usuários da rede de recreação abandonasse as ofertas baseadas em texto da Telnet para as páginas “multimídia” disponíveis na World Wide Web. No início dos anos 90, você ainda podia usar o Telnet para [acessar arquivos da Biblioteca do Congresso dos Estados Unidos](https://www.loc.gov/loc/lcib/93/9310/remote.html) , acessar [as últimas manchetes e comunicados de imprensa da NASA](http://info.cern.ch/hypertext/DataSources/Yanoff.html) e até ler versões ASCII das decisões da Suprema Corte.

Mas a simplicidade do Telnet foi, em última análise, sua ruína. Em 2007, a [revista Wired observou](https://www.wired.com/2007/04/telnet-dead-at-/) que o Telnet não estava incluído no sistema operacional Windows Vista da Microsoft. "Concebido em um momento mais simples, o Telnet não tem criptografia e não chega nem perto de atender aos modernos padrões de segurança para efetuar logon em uma máquina remota". A maioria das conexões remotas agora acontece usando um cliente SSH mais seguro como o PuTTY.

Mas é bom ver que todas essas décadas depois, o antigo protocolo Telnet ainda tem alguns fãs restantes.

[Fonte henewstack:](https://thenewstack.io/the-lost-worlds-of-telnet/)