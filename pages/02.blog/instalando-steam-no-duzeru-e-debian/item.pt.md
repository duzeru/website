---
title: 'Instalando Steam no DuZeru e Debian'
media_order: '001.png,002.png,003.png,004.png'
shortcode-citation:
    items: cited
    reorder_uncited: true
visible: true
feed:
    limit: 10
---

Executar os comandos:

```
sudo apt-get update && sudo apt-get install steam-launcher -y
```

Abra o ícone do menu e abrirá uma tela de terminal, insira a senha e pressione enter
![](001.png)
![](002.png)

Ainda continuando a instalação, baixará o conteúdo da Steam...
![](003.png)

Pronto, a Steam está instalada e pronta para utilizar.
![](004.png)