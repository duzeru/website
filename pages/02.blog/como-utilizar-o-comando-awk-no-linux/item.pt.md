---
title: 'Como utilizar o comando awk no linux'
media_order: 'awk.jpg,magic.gif,magic2.gif,aplausos.gif'
shortcode-citation:
    items: cited
    reorder_uncited: true
visible: true
feed:
    limit: 10
---

A linguagem de programação **AWK** (Sim é uma linguagem) foi criada em 1977 pelos cientistas Alfred Aho, Peter J. Weinberger e Brian Kernighan no laboratório Bell Labs. A palavra AWK é uma abreviatura das iniciais dos sobrenomes dos criadores da linguagem (Aho, Weinberger e Kernighan).

A linguagem é interpretada linha por linha e tem como principal objetivo deixar os scripts de Shell mais poderosos e com muito mais recursos sem utilizar muitas linhas de comando, podendo resolver infinidades de problemas do dia-a-dia do desenvolvedor nesses sistemas operacionais.

#### Problemática

Geralmente encontramos muita teoria, mas nada melhor que unir a teoria com a prática.

Vamos supor que você está encubido de enviar uma lista de todos os usuários do sistema. e esta lista pode ser encontrada no arquivo `/etc/passwd` mas este arquivo além dos nomes dos usuários também atribui diversos dados, como proceder?

**Exemplo do arquivo /etc/passwd**
```
root:x:0:0:root:/root:/bin/zsh
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
```

#### Prática
Bem, há um padrão neste arquivo, perceba que ele tem dois pontos (**:**) separando uma informação de outra, neste caso utilizaremos a seguinte linha com o **awk**
```
awk -F":" '{print $1}' /etc/passwd
```

O resultado é o seguinte:
```
root
daemon
bin
sys
```
![](magic.gif)

#### Explicação

Bem, não se espante pois vou explicar agora:

* **awk**  Chama o comando
* **-F ""** Especificar o separador dentro das aspas duplas
* **'{print $1}'** há este campo realmente necessário para efetuar o print de algo '{ print }' e a variável $1 é o primeiro campo antes do separador, neste caso o nome dos usuários.
*  **/etc/passwd** O nome do arquivo a ser tratado.

Agora que você têm uma luz, vamos para outros exemplos

#### Exemplos:

Você pode visualizar mais de um campo incluindo mais de uma variável, no exemplo abaixo, listamos o campo do usuário com a variável **$1** e o PATH com a variável **$6**
```
awk -F":" '{print $1 $6}' /etc/passwd
```
O resultado é:
```
root/root
daemon/usr/sbin
bin/bin
sys/dev
```

temos uma saída bacana, mas os dois campos estão juntos e podemos melhorar a saída adicionando o seguinte:
```
awk -F":" '{print "User:"$1 " PATH:"$6}' /etc/passwd
```

Entre aspas duplas antes da variável,  pode escrever qualquer coisa, neste caso escrevi **"User:"** e **" PATH:"**. A saída do comando é:
```
User:root PATH:/root
User:daemon PATH:/usr/sbin
User:bin PATH:/bin
User:sys PATH:/dev
```

Agora sim, está bem mais organizado não acha?

![](magic2.gif)

#### Enviando os dados tratados para um arquivo.

Já que sabemos tratar os dados do arquivo, vamos salvar estes dados em um arquivo e entregar o relatório pedido, é ridiculamente simples, basta acrescentarmos o caractere **>** após o comando e inserir o caminho e nome do arquivo que desejamos:

```
awk -F":" '{print "User:"$1 " PATH:"$6}' /etc/passwd > /home/$USER/relatorio.txt
```

Pronto, já temos o relatório para entragar que foi criado de forma simples, indolor e incolor.

#### Exemplo awk avançado

Agora vamos supor que você se depara com a seguinte problemática, precisa listar tudo que foi criado no ano de 2019 no diretório do usuário **$USER** façamos o seguinte:
```
ls -l /home/$USER | awk '{if ($8 == "2019") print ($0)}'
```
No comando acima listamos todos os arquivos do usuário com **ls**, utilizamos o pipe "**|**" para logo em seguida tratar a saída com o awk. Dentro do **awk** adicionamos uma condição **if** de que no campo **$8** aprensente somente o que tiver **2019**. A saída desde comando para mim foi:
```
claudio@PC: ls -l /home/$USER | awk '{if ($8 == "2019") print ($0)}'
drwxr-xr-x  2 claudio claudio    4096 jul 15  2019 DevOps
drwxr-xr-x  3 claudio claudio    4096 jul 29  2019 Edraw
drwxr-xr-x  2 claudio claudio    4096 jan 30  2019 Modelos
drwxr-xr-x  2 claudio claudio    4096 fev 20  2019 Público
```

![](aplausos.gif)

Para saber as sintaxes do comando awk, basta executar:
```
awk --help
```

Fontes: 
[https://pt.wikipedia.org/wiki/AWK](https://pt.wikipedia.org/wiki/AWK)
[http://cesarakg.freeshell.org/awk-1.html](http://cesarakg.freeshell.org/awk-1.html)