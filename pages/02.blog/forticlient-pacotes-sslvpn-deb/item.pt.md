---
title: 'Forticlient - pacotes SSLVPN .deb'
media_order: LinuxSSLVPN_createprofile.png
shortcode-citation:
    items: cited
    reorder_uncited: true
visible: true
feed:
    limit: 10
---

Forticlient - SSLVPN é um cliente VPN para conectar-se a dispositivos Fortigate com o mínimo de esforço, empacotado aqui para Ubuntu e Debian.

Oficialmente, existe apenas um pacote tar.gz genérico disponível. Como uso o Ubuntu na maioria das vezes, decidi criar pacotes .deb para o Ubuntu 32 / 64bit com um bom ícone na área de trabalho para iniciar:)

Esses pacotes também devem funcionar no debian, mas eu não testei isso agora (seguirá).

Para atualizações, basta baixar o novo pacote e instalá-lo, o gerenciador de pacotes fará a atualização para você.

Vou compartilhar meus pacotes aqui para você baixar:

Atualização 14.4.2017 (construída no Ubuntu 16.04):

[Cliente SSLVPN 4.4.2333-1 32 bits](https://hadler.me/files/forticlient-sslvpn_4.4.2333-1_i386.deb)
[Cliente SSLVPN 4.4.2333-1 64 bits](https://hadler.me/files/forticlient-sslvpn_4.4.2333-1_amd64.deb

Versões antigas
[Cliente SSLVPN 4.4.2332-1 32 bits](https://hadler.me/files/forticlient-sslvpn_4.4.2332-1_i386.deb)
[Cliente SSLVPN 4.4.2332-1 64 bits](https://hadler.me/files/forticlient-sslvpn_4.4.2332-1_amd64.deb)


Fonte: https://hadler.me/linux/forticlient-sslvpn-deb-packages/