---
title: 'Instalar o Wine 5 no Debian 10, Debian 9, Ubuntu e Mint'
media_order: Wine_Logo.png
shortcode-citation:
    items: cited
    reorder_uncited: true
visible: true
feed:
    limit: 10
---

Este guia abordará a instalação do Wine 5.

Wine é um software de código aberto que permite executar aplicativos do Microsoft Windows no Linux. Ao alavancar sua   biblioteca Winelib , você pode compilar aplicativos do Windows para ajudar a transportá-los para sistemas semelhantes ao Unix.

#### Etapa 1: ativar a arquitetura de 32 bits
Se você estiver executando um sistema de 64 bits, ative o suporte para aplicativos de 32 bits.
```
sudo dpkg --add-architecture i386
```
O comando acima não retornará nenhuma saída.

#### Etapa 2: adicionar repositório WineHQ
Iremos puxar os pacotes mais recentes do Wine do repositório WineHQ que são adicionados manualmente.

Primeiro, importe a chave GPG:
```
sudo apt update
sudo apt -y install gnupg2 software-properties-common
wget -qO - https://dl.winehq.org/wine-builds/winehq.key | sudo apt-key add -
```
Você deve receber " OK " na saída.

Adicione o repositório do Wine executando o seguinte comando:
```
sudo apt-add-repository https://dl.winehq.org/wine-builds/debian/
```
O comando adicionará uma linha ao arquivo **/etc/apt/sources.list**.

#### Etapa 3: Instale o Wine 5

Após a configuração do repositório APT, a etapa final é a instalação real do Wine 5 no Debian 10/9.

Adicione o repositório Wine **DuZeru ou Debian 10**:
```
wget -O- -q https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/Debian_10/Release.key | sudo apt-key add -    
echo "deb http://download.opensuse.org/repositories/Emulators:/Wine:/Debian/Debian_10 ./" | sudo tee /etc/apt/sources.list.d/wine-obs.list
```

Adicione o repositório Wine **DuZeru ou Debian 9**:
```
wget -O- -q https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/Debian_9.0/Release.key | sudo apt-key add -    
echo "deb http://download.opensuse.org/repositories/Emulators:/Wine:/Debian/Debian_9.0 ./" | sudo tee /etc/apt/sources.list.d/wine-obs.list
```

Em seguida, instale o Wine da ramificação estável:
```
sudo apt update
sudo apt install --install-recommends winehq-stable
```

Depois da instalação. verifique a versão instalada.
```
wine --version
```

#### Etapa 4: Usando o Wine no Debian
Para uso básico de wine, consulte a página de ajuda.
```
wine --help
```

O exemplo abaixo é usado para executar o editor do Notepad ++ no Linux.
```
cd ~/Downloads
wget https://notepad-plus-plus.org/repository/7.x/7.7/npp.7.7.Installer.exe
wine ./npp.7.7.Installer.exe
```

[Fonte do artigo](https://computingforgeeks.com/how-to-install-wine-on-debian/)