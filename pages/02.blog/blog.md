---
title: Blog
shortcode-citation:
    items: cited
    reorder_uncited: true
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
feed:
    limit: 10
---

bora