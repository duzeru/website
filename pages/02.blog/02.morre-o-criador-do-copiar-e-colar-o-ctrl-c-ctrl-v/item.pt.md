---
title: 'Morre Larry Tesler o criador do ''copiar e colar'' (CTRL C, CTRL V)'
media_order: 'larry-tesler.jpg,larry-tesler2.jpg'
shortcode-citation:
    items: cited
    reorder_uncited: true
feed:
    limit: 10
---

> Cientista da computação criou os termos ao desenvolver um editor de textos em 1975. Suas ideias também influenciaram a forma como software se comporta até hoje
> 

Morreu nesta segunda-feira (17), aos 74 anos, o cientista da computação [Larry Gordon Tesler](http://www.nomodes.com/Larry_Tesler_Consulting/Home.html). Você pode não reconhecer o nome, mas certamente usa os frutos de seu trabalho todos os dias. Entre outras contribuições ele foi o inventor dos termos “recortar”, “copiar” e “colar”, originalmente relacionados a um editor de textos que criou em 1975.

![](larry-tesler2.jpg)

 Larry Tesler , falecido na segunda-feira, pode não ser um nome familiar, como Steve Jobs ou Bill Gates, mas suas contribuições para tornar os computadores e dispositivos móveis mais fáceis de usar são o destaque de uma longa carreira que influencia a computação moderna.
 
 Nascido em 1945 em Nova York, Tesler estudou ciência da computação na Universidade de Stanford. Em 1973, Tesler aceitou um emprego no Centro de Pesquisa Xerox Palo Alto (PARC), onde trabalhou até 1980. O Xerox PARC é famoso por desenvolver a interface gráfica do usuário acionada por mouse. Tesler trabalhou com Tim Mott para criar um processador de texto chamado Gypsy, mais conhecido por cunhar os termos “recortar”, “copiar” e “colar” quando se trata de comandos para remover, duplicar ou reposicionar pedaços de texto.

Em 1980, Tesler migrou para a Apple Computer, onde trabalhou até 1997. Ao longo dos anos, ele ocupou inúmeros cargos na empresa, incluindo vice-presidente da AppleNet. (Sistema interno de rede local da Apple que acabou sendo cancelado) e até atuou como cientista-chefe da Apple, cargo que Steve Wozniak ocupou uma vez antes de deixar a empresa.

Além de suas contribuições para alguns dos hardwares mais famosos da Apple, Tesler também era conhecido por seus esforços para tornar o software e as interfaces de usuário mais acessíveis. Além das onipresentes terminologias "recortar", "copiar" e "colar", Tesler também foi um defensor de uma abordagem ao design da interface do usuário conhecida como computação sem modelagem, que se reflete em seu [site pessoal](http://www.nomodes.com/Larry_Tesler_Consulting/Home.html).

Depois de deixar a Apple em 1997, Tesler co-fundou uma empresa chamada Stagecast Software, que desenvolveu aplicativos que tornavam mais fácil e acessível para as crianças aprenderem conceitos de programação. Em 2001, ingressou na Amazon e, eventualmente, tornou-se vice-presidente de compras no país. Em 2005, mudou para o Yahoo, onde liderou o grupo de design e experiência do usuário da empresa. Em 2008, tornou-se sócio de produtos da 23andMe. [De acordo com seu currículo](http://www.nomodes.com/Larry_Tesler_Consulting/1997-Now.html) , Tesler deixou o 23andMe em 2009 e, a partir de então, focou-se principalmente no trabalho de consultoria.

Embora haja, sem dúvida, inúmeras outras contribuições que Tesler fez para a computação moderna como parte de seu trabalho em equipes da Xerox e da Apple que talvez nunca venham à tona, suas contribuições conhecidas são imensas. Tesler é uma das principais razões pelas quais o computador foi transferido dos centros de pesquisa para as residências.

Obrigado Larry pelas suas contribuições. 

[Foto Wikimedia](https://upload.wikimedia.org/wikipedia/commons/c/c3/Larry_Tesler_Smiles_at_Whisper.jpeg)

[Fonte gizmodo](https://gizmodo.com/larry-tessler-modeless-computing-advocate-has-passed-1841787408)