---
title: 'Num fim de semana, programadores tchecos criaram um site de graça pelo qual ministro planejava gastar 16 milhões de euros'
media_order: '001.png,002.jpeg,003.jpg'
shortcode-citation:
    items: cited
    reorder_uncited: true
visible: true
feed:
    limit: 10
---

Voluntários consideraram o sistema caro demais e simplesmente realizaram o serviço para o governo, apontando-o como despesa injustificada.

Um grupo de programadores tchecos fez e entregou ao governo um sistema de informática de graça, que tinha sido encomendado a outra empresa. Os desenvolvedores fizeram isso em protesto contra o “desperdício” do Ministério dos Transportes. Na opinião deles, as autoridades firmaram um contrato excessivamente alto. A história chegou ao primeiro-ministro tcheco e ele demitiu o ministro dos Transportes.

Tudo começou com uma compra sem licitação para a criação de um serviço para a venda de autorizações eletrônicas de viagem para rodovias com pedágio. O pedido de 400 milhões de coroas tchecas (quase 16 milhões de euros) incluía o desenvolvimento de uma loja online e dois aplicativos para iOS e Android. O contrato de quatro anos para o desenvolvimento e suporte do site foi assinado com a Asseco Central Europe — sem edital público, repetimos.

O negócio provocou indignação na comunidade de TI tcheca devido ao “desperdício óbvio” por parte do governo. O proprietário da Actum Digital, Tomas Vondracek, chamou a proposta de “absurda” e acrescentou que o trabalho poderia ser feito “em alguns dias”.

Vondrachek convidou programadores locais a se unirem a fim de criarem um sistema análogo. Eles planejaram fazer tudo durante um fim de semana no escritório da Actum Digital em Praga e simplesmente transferir o código para o governo tcheco como presente e um exemplo de como os impostos dos cidadãos são gastos.

“Fomos motivados pelo desejo de resistir ao sistema de superestimar contratos estatais, onde há repetidos abusos. Neste caso, nosso dinheiro simplesmente entrava pelo cano”, disse.
![](002.jpeg)

_Tomas Vondrachek | tjournal.ru_

Mais de 300 programadores da República Tcheca responderam às chamadas nas redes sociais. Na sexta-feira, 24 de janeiro, 60 técnicos se reuniram no escritório de Vondrachek para começar a trabalhar. No domingo (26) à noite, eles criaram a plataforma [fairznamka.cz](https://fairznamka.cz/), que atendia aos requisitos do serviço de 16 milhões de euros.

O primeiro-ministro tcheco Andrei Babish veio ver o trabalho dos programadores. “Estou chocado com o que está acontecendo”, disse ele a repórteres. Os voluntários desenvolveram o serviço em 22 horas de trabalho.

Já na segunda-feira, 27 de janeiro, o site ganhou um módulo público de teste. O aplicativo ainda não está conectado ao sistema estatal, mas cumpre todas as funções necessárias. Durante o primeiro dia, o serviço foi visitado por mais de 175 mil pessoas que experimentaram a carga no site e fizeram as primeiras compras. Os pagamento pelo acesso à rodovia foram feitos simbolicamente. Mas quem quisesse, poderia pagar de verdade, o dinheiro arrecadado iria para organizações de sem-teto do país.

O governo tcheco inicialmente se ofereceu para pagar pelo sistema, mas depois aceitou o site fairznamka.cz de presente, como o pretendido pelos programadores. Babish enfatizou que o Ministério dos Transportes realizará uma licitação pública para encontrar uma empresa que implante o sistema ainda este ano. Espera-se que o valor do contrato seja muitas vezes menor que o original. Vondraček estimou que a licitação terá um valor de 25% do valor inicial e sublinhou que sua empresa não participará do novo edital.

O ministro dos Transportes tcheco, Vladimir Kremlik, foi demitido. Representantes da empreiteira Asseco Central Europe disseram que estavam prontos para rescindir o contrato sem reivindicar compensação. Após a renúncia, Kremlik observou que respeita a decisão do primeiro-ministro, mas acrescentou que suas prioridades eram criar um sistema “sem demora”…

Segundo Vondrachek, o desenvolvimento voluntário é um importante precedente, após o qual a República Tcheca, refletirá sobre o valor de cada serviço. “Acho que as licitações de TI excessivamente altas acabaram”, acrescentou.
![](003.jpg)
_O alegre grupo de programadores | tjournal.ru_



[Fonte:](https://tjournal.ru/internet/139477-cheshskie-programmisty-besplatno-i-za-vyhodnye-sdelali-sayt-na-kotoryy-ministr-hotel-potratit-16-mln-evro-ego-uvolili) 