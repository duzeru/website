---
title: 'Autenticação LDAP (Active Directory) no DuZeru, Debian, Ubuntu, Mint e derivados'
media_order: ldap.png
shortcode-citation:
    items: cited
    reorder_uncited: true
feed:
    limit: 10
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: '1'
---

## Sumário 
 - [ Objetivo ](#Objetivo) 
 - [ Conceitos e Definições ](#Conceitos) 
 - [ Fluxo de atividades ](#Atividades) 
 - [ Referências ](#Referencias) 

#### Objetivo: <a name='Objetivo'></a> 
Ingressar no domínio do Windows Active Directory com o Samba Winbind.

#### Conceitos e Definicões: <a name='Conceitos'></a> 
 Este procedimento abrange Servidores com Sistemas Operacionais baseados no Debian como Ubuntu, Mint ou outro derivado. 

#### Fluxo de atividades: <a name='Atividades'></a> 
 
**1- Instale o Winbind e samba**
 ```
 root@smb:~# apt-get install -y install samba winbind libpam-winbind libnss-winbind krb5-config resolvconf
# specify Realm
 +------------------+ Configuring Kerberos Authentication +------------------+
 | When users attempt to use Kerberos and specify a principal or user name   |
 | without specifying what administrative Kerberos realm that principal      |
 | belongs to, the system appends the default realm.  The default realm may  |
 | also be used as the realm of a Kerberos service running on the local      |
 | machine.  Often, the default realm is the uppercase version of the local  |
 | DNS domain.                                                               |
 |                                                                           |
 | Default Kerberos version 5 realm:                                         |
 |                                                                           |
 | SRV.WORLD________________________________________________________________ |
 |                                                                           |
 |                                  <Ok>                                     |
 |                                                                           |
 +---------------------------------------------------------------------------+
# specify the hostname of AD DS
     +--------------+ Configuring Kerberos Authentication +---------------+
     | Enter the hostnames of Kerberos servers in the SRV.WORLD           |
     | Kerberos realm separated by spaces.                                |
     |                                                                    |
     | Kerberos servers for your realm:                                   |
     |                                                                    |
     | fd3s.srv.world____________________________________________________ |
     |                                                                    |
     |                               <Ok>                                 |
     |                                                                    |
     +--------------------------------------------------------------------+
# specify the hostname of AD DS
 +------------------+ Configuring Kerberos Authentication +------------------+
 | Enter the hostname of the administrative (password changing) server for   |
 | the SRV.WORLD Kerberos realm.                                             |
 |                                                                           |
 | Administrative server for your Kerberos realm:                            |
 |                                                                           |
 | fd3s.srv.world___________________________________________________________ |
 |                                                                           |
 |                                  <Ok>                                     |
 |                                                                           |
 +---------------------------------------------------------------------------+ 
 ```
 
 **2- Configurar o Winbind**
 
 ```
 root@smb:~# vi /etc/samba/smb.conf
 
[global]

workgroup = 
   password server = 
   realm = 
   security = ads
   idmap config * : range = 16777216-33554431
   template shell = /bin/bash
   template homedir = /home/%U
   kerberos method = secrets only
   winbind use default domain = yes
   winbind offline logon = false

root@smb:~# vi /etc/nsswitch.conf
# line 7: add like follows
passwd:		  compat winbind
group:			compat winbind
shadow:       compat winbind

root@smb:~# vi /etc/pam.d/common-session
# add to the end if you need (auto create a home directory when initial login)
session optional        pam_mkhomedir.so skel=/etc/skel umask=077

root@smb:~# systemctl restart  smbd
 ```
**3- Ingresse o usuário na rede LDAP**
 ```
root@smb:~# net ads join -U USUARIO_DE_REDE
Enter Administrator's password:
Using short domain name -- 
Joined 'SERVER' to dns domain 'anvisa.gov.br'

root@smb:~# systemctl restart winbind
# try to show domain users info

root@smb:~# wbinfo -u
administrator
guest
defaultaccount
serverworld
krbtgt
 ```

**4- Limitar o acesso ao servidor para apenas um ou vários grupos**

```
root@smb:~# vim /etc/ssh/sshd_config

Port 22
Protocol 2
SyslogFacility AUTHPRIV
PubkeyAuthentication yes
AuthorizedKeysFile .ssh/authorized_keys
PasswordAuthentication yes
PermitRootLogin yes
#StrictModes yes
#MaxAuthTries 3
#MaxSessions 5
ChallengeResponseAuthentication yes
GSSAPIAuthentication yes
GSSAPICleanupCredentials yes
UsePAM yes
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS
X11Forwarding no
UseDNS no
Subsystem sftp /usr/libexec/openssh/sftp-server
AllowGroups root
Banner /etc/issue
```


![](ldap.png)
  
#### Referências: <a name='Referencias'></a> 
 - [https://www.server-world.info/en/note?os=Debian_9&p=samba&f=3](https://www.server-world.info/en/note?os=Debian_9&p=samba&f=3)  