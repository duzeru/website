---
title: 'Estratégia de Container'
media_order: post-docker.png
shortcode-citation:
    items: cited
    reorder_uncited: true
visible: true
feed:
    limit: 10
---

Conteinerização de aplicações é uma tendência que vem crescendo a passos largos conforme se consolidam os métodos ágeis de construção de software, DevOps e microsserviços em containers. Porém, muitos times de TI têm dúvidas sobre como implantar uma estratégia de container bem-sucedida.

Por isto, preparamos este artigo que tem a finalidade de compartilhar as melhores práticas para uma boa estratégia de conteinerização de aplicações.

Ficou curioso?

#### Por que adotar uma estratégia de containers?

A tendência é abrangente ao redor do mundo: até 2020 mais de 50% das empresas globais estarão executando aplicativos com containers em produção. Isto significa um aumento significativo em relação aos menos de 20% atuais. Segundo o Gartner, e a IDC (International Data Corporation),  até 2021, mais de 95% dos novos micro-serviços serão implantados em containers. 

O Gartner também sinaliza que equipes de infraestrutura e operações (I&O) estão sob pressão para oferecer aplicativos com mais rapidez. Isso porque empresas perceberam que a criação de produtos e serviços de software se traduz em aumento da participação de mercado. 

A utilização de containers é o caminho para a organização e a modernização do legado de aplicativos, e para permitir que se criem novos apps nativos da cloud, que são escaláveis e ágeis. Os frameworks de container, como Docker, fornecem uma maneira padronizada de empacotar os aplicativos – incluindo código, tempo de execução e bibliotecas – e executá-los durante todo o ciclo de vida do desenvolvimento de software.

O ecossistema dos containers é maduro, garantindo uma redução considerável de custos no desenvolvimento e produção do projeto, bem como reduzindo o tempo de entrega e aumentando a eficiência com escalabilidade e alta disponibilidade.

Pensando nisso, nosso time Vertigo, que é parceiro da Docker, pioneira na tecnologia de conteinerização (conjunto de técnicas que transformam em containers suas aplicações), listou abaixo as premissas básicas para montar uma estratégia de sucesso no processo de implementação dos containers.

#### Passo a passo para montar uma estratégia de 

1. Segurança
Para a integridade de sistemas distribuídos em contêineres é fundamental a proteção dos dados e isolamento dos volumes, dos quais os sistemas dependem. Um sistema operacional minimalista pode ser usado como sistema operacional host e os contêineres precisam ser monitorados de forma contínua contra vulnerabilidades e malwares, para garantir uma entrega de serviços confiável.

2. Monitoramento de Container:
A popularidade de aplicativos nativos da nuvem muda o foco de um monitoramento direto (baseado no host) para um orientado por serviços para garantir a conformidade com os acordos de nível de serviço de resiliência e desempenho. É fundamental implementar ferramentas integradas que possam fornecer monitoramento do container e do nível de serviço. Além disto, vinculá-las aos orquestradores de containers para obter métricas em outros componentes, para uma melhor visualização e analytics (dashboard de análise). Esta é uma ferramenta essencial de APM (Application Performance Monitoring)  para que o DevOps possa rapidamente desenvolver e implantar aplicativos inovadores na nuvem, incorporando novas abordagens para captura de métricas, correlação, análise, rastreamento de transações e visualização. Temos no nosso blog artigos que abordam esse tópico de forma mais abrangente.

3. Rede:
A natureza efêmera e o ciclo de vida dos containers são um desafio à uma plataforma de rede tradicional. As equipes de infraestrutura e operações devem eliminar o trabalho manual em ambientes em container, abraçando a agilidade dos orquestradores por meio da automação de rede, dando então aos desenvolvedores ferramentas adequadas e flexibilidade suficiente.

4. Armazenamento:
Uma vez que os containers são transitórios, os dados devem ser desassociados do container para que os arquivos persistam e estejam protegidos mesmo depois que o container seja descartado. O administrador de volumes, podendo ser nuvem ou local, consegue gerenciar múltiplos acessos de containers, garantindo integridade das informações e permitindo o acesso sob demanda dos containers em execução no momento do projeto.

5. Orquestração de Container:
As ferramentas para gerenciamento de container são os “cérebros” de um sistema distribuído, tomando decisões sobre a descoberta de componentes da infraestrutura de um serviço, equilibrando as cargas de trabalho com recursos desse mesmo ambiente, fornecendo ou não infraestruturas, clusterizando serviços, failover automático. Você poderá se aprofundar melhor sobre orquestração lendo nossos artigos específicos sobre esse assunto.

6. Gerenciamento de ciclo de vida do container:
Os containers possuem alta disponibilidade e escalabilidade. O gerenciamento do ciclo de vida do container pode ser automatizado por meio de uma estreita conexão com processos de integração contínua, juntamente com ferramentas de automação para facilitar a implantação de infraestrutura e das tarefas operacionais.

#### Conclusão:
Esses são apenas alguns dos passos para o sucesso na implementação da tecnologia de containers. É importante ressaltar, porém, que – como tudo no mundo da tecnologia – as possibilidades são infinitas. Por isso, é importante que cada empresa encontre a sua melhor forma de empregar diferentes metodologias para o melhor uso da ferramenta, tendo sempre em vista a integração de equipes e a cultura do DevOps.

Como resultado da conteinerização, obtemos uma coletânea das melhores práticas de tudo aquilo que já vinha sendo aplicado, com as devidas adaptações e possíveis correções de falhas experimentadas. Mesmo assim, a conteinerização não é uma visão fechada e continua evoluindo conforme vão se identificando possibilidades e necessidades, como tudo no mundo dinâmico da tecnologia.

E aí, gostou destas dicas?

Se sim, não deixe de conferir o ebook abaixo com as principais dúvidas que os líderes de TI tem sobre Docker!

