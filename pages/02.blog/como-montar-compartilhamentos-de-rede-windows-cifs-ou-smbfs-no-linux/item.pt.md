---
title: 'Como montar compartilhamentos de rede Windows (CIFS ou SMBFS) no Linux'
media_order: cifs.jpg
shortcode-citation:
    items: cited
    reorder_uncited: true
visible: true
feed:
    limit: 10
---

![](cifs.jpg)

Se você chegou até aqui, então passou pela mesma dúvida que eu tive e precisei recorrer ao Google: como faço pra montar um diretório compartilhado do Windows no meu servidor Linux?
Sendo esta sua dúvida ou não, vamos logo ao que interessa.

Para solucionarmos precisamos dos requisitos:
* Acesso root no Linux
* Caminho do compartilhamento (//servidor/compartilhamento ou //192.168.0.1/compartilhamento)
* Usuário e senha do Windows com permissão de acesso ao compartilhamento

## Procedimento

Faça login no Linux como root ou execute os comandos com o sudo

Crie um diretório para montar o compartilhamento:
```
mkdir -p /mnt/windows
```
Use o comando mount para montar o compartilhamento no diretório criado:
```
mount -a
```

##### Montagem manual

Para montar casualmente na sua sessão (caso reinicie a sessão não estará mais montado) execute:
```
mount -t cifs //servidor/compartilhamento -o username=usuario,password=senha /mnt/windows
```

Acesse o compartilhamento com os comandos:
```
cd /mnt/windows ; ls -l
```

Pronto, o diretório compartilhado já está montado no seu servidor Linux!

Para fixar a montagem, Se você quiser montar o compartilhamento automaticamente na inicialização do seu servidor linux, inclua esta linha no final do seu arquivo /etc/fstab:

```vim /etc/fstab```

```
//servidor/compartilhamento /mnt/windows cifs rw,username=usuario,password=senha 0 0
```

Caso não funcione, deve ser verificado a permissão do usuário no diretório Windows.