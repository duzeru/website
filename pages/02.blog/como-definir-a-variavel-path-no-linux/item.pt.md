---
title: 'Como definir a variável PATH no Linux'
media_order: path.jpeg
shortcode-citation:
    items: cited
    reorder_uncited: true
visible: true
feed:
    limit: 10
---

No Linux, seu PATH é uma lista de diretórios nos quais o shell procurará arquivos executáveis quando você emitir um comando sem um caminho. A variável PATH geralmente é preenchida com alguns diretórios padrão, mas você pode definir a variável PATH para o que quiser.

_Quando um nome de comando é especificado pelo usuário ou uma chamada exec é feita a partir de um programa, o sistema procura `$PATH`, examinando cada diretório da esquerda para a direita na lista, procurando um nome de arquivo que corresponda ao nome do comando. Uma vez encontrado, o programa é executado como um processo filho do shell de comando ou programa que emitiu o comando.
**-Wikipedia**_

Neste breve tutorial, discutiremos como adicionar um diretório ao seu PATH e como tornar as alterações permanentes. Embora não exista uma maneira nativa de excluir um diretório padrão do seu caminho, discutiremos uma solução alternativa. Terminaremos criando um script curto e colocando-o em nosso PATH recém-criado para demonstrar os benefícios da variável PATH.

#### Erros típicos causados por um arquivo que não está no seu caminho
Quando você tenta executar um comando que possui um local desconhecido (nenhum caminho especificado) para o shell, ele começará a pesquisar nos diretórios listados no seu PATH da esquerda para a direita. Se nenhum comando existir com o nome que você forneceu, o shell exibirá o seguinte erro.
```
$ gocrazy 
bash: gocrazy: comando não encontrado…
```

Você pode pesquisar os diretórios em seu PATH usando o comando `which`. Se você emitir o comando which com um argumento que o shell não pode encontrar no seu caminho, ele exibirá o seguinte erro.
```
$ which gocrazy
 /usr/bin/which: no gocrazy in (/usr/share/Modules/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/home/savona/.local/bin:/home/savona/bin)
 ```
 Mesmo que o arquivo exista em um dos diretórios do seu PATH, ele deve ser executável para ser encontrado.
 
#### Exibir o valor da variável PATH
Você pode verificar seu caminho atual usando echo para imprimir o valor de $PATH na tela ( stdout ).
```
$ echo $PATH 
/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/savona/.local/bin:/home/savona/bin
```
#### Adicionando um diretório ao seu PATH
Se você deseja adicionar um diretório ao seu caminho, pode usar o comando export Por exemplo, se você deseja adicionar um diretório chamado scripts que reside em seu diretório pessoal, você pode adicioná-lo da seguinte maneira:
```
export PATH=$PATH:/home/user/scripts
```
* **export** = Diz ao bash para disponibilizar a variável ambiental para qualquer processo filho.
* **PATH** = Informa ao bash que você está configurando a variável $ PATH
* **$PATH** = Coloca o valor atual da variável PATH na variável recém-definida.
* **:** = É um separador ou delimitador
* **/home/user/scripts** = É o diretório que estamos adicionando

Agora você adicionou /home/user/scripts ao seu PATH.
```
echo $PATH 
/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/savona/.local/bin:/home/user/bin:/home/user/scripts
```

Agora, o shell procurará em `/home//user/scripts` quando eu chamar um executável a partir da linha de comando.

O uso da exportação funciona muito bem para uma alteração temporária na variável. As alterações que fizemos usando a exportação não estarão disponíveis se abrirmos um novo shell ou reiniciarmos. A exportação apenas o disponibiliza para o shell atual, processos filho ou shells que são gerados pelo shell atual (sub shells).

#### Faça uma alteração persistente na variável PATH

Para fazer alterações persistentes na variável PATH, precisamos adicioná-la a um arquivo. Aqui vamos adicioná-lo ao arquivo **~/.bash_profile**. Este arquivo é lido toda vez que você efetua login, para que a variável PATH esteja configurada e pronta.

**NOTA**: O til (~) é uma função especial que significa seu diretório pessoal.

Abra ~/.bash_profile no seu editor favorito e adicione a mesma linha de exportação inserida acima.

```
export PATH=$PATH:~/scripts
```

Salve e feche o arquivo.
Como alternativa, você pode simplesmente repetir a linha no arquivo da seguinte maneira:
```
echo 'export PATH = $PATH:~/scripts' >> ~/.bash_profile
```
Você precisa originar o arquivo bash_profile se quiser disponibilizar as alterações no shell atual.
```
$ source ~/.bash_profile
```
Você só precisa originar o arquivo no shell atual. Se você abrir um novo shell, efetue logout e logon novamente ou reinicie o bash_profile será lido e seu PATH será definido.


#### Conclusão
Provavelmente fiquei um pouco sem ar por lá, peço desculpas. Mas demonstramos como exibir e manipular a variável PATH. Também mostramos por que adicionar algo ao seu caminho é conveniente.

Normalmente, uso apenas ~/.local/bin para todos os meus scripts. Ele mantém meu diretório pessoal organizado e ainda permite que eu chame meus scripts sem usar um caminho absoluto.

Fonte: [https://www.putorius.net/set-path-variable-linux.html](https://www.putorius.net/set-path-variable-linux.html)