---
title: 'Guia de bolso comandos Linux'
media_order: basic-commands.png
shortcode-citation:
    items: cited
    reorder_uncited: true
feed:
    limit: 10
---

Quando está iniciando no Linux, nada como ter um guia de bolso que seja simples e objetivo.

![](basic-commands.png)