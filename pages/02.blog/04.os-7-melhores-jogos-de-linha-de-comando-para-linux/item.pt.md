---
title: 'Os 7 melhores jogos de linha de comando para Linux'
media_order: 'capa.png,bastet.jpg,games2.jpg,games3.jpg,games4.jpg,games6.jpg,games7.jpg'
shortcode-citation:
    items: cited
    reorder_uncited: true
feed:
    limit: 10
---

Existem distribuições Linux dedicadas  para jogos. Sim, eles existem. Mas, não vamos ver as distribuições de jogos Linux hoje, vamos falar de algo significativamente interessando que é "**JOGAR VIA TERMINAL**".

O Linux tem uma vantagem adicional sobre o seu homólogo do Windows. Ele possui o poderoso terminal Linux. Você pode fazer muitas coisas no terminal, incluindo jogar jogos de linha de comando .

Sim, amantes incondicionais do terminal, se reúnem. Os jogos de terminal são leves, rápidos e muito divertidos de jogar. E o melhor de tudo, você tem muitos jogos retrô clássicos no terminal Linux.

**1. Bastet**

Quem não passou horas juntos jogando Tetris? Simples, mas totalmente viciante. Bastet é o Tetris do Linux.
![](bastet.jpg)

Use o comando abaixo para obter Bastet:
```
sudo apt install bastet
```
Para jogar Use a barra de espaço para girar os tijolos e as teclas de seta para guiar, inicie executando o comando:
```
bastet
```

**2. Ninvaders**
Invasores do espaço. Lembro-me de lutar por uma pontuação alta com meu irmão nisso. Um dos melhores jogos de arcade por aí.

![](games2.jpg)

Copie e cole o comando para instalar o Ninvaders.
```
sudo apt-get install ninvaders
```
Para jogar execute o comando:
```
ninvaders
```
Teclas de seta para mover a nave espacial. Barra de espaço para atirar nos alienígenas.

**3. Pacman4console**
Sim, o rei do arcade está aqui. Pacman4console é a versão terminal do popular jogo de arcade Pacman.

![](games3.jpg)

Use o comando para obter pacman4console:
```
sudo apt-get install pacman4console

```
Para jogar execute o comando:
```
pacman4console
```

**4. nSnake**
Lembre-se do jogo da serpente nos antigos telefones Nokia?

Esse jogo me manteve ligado ao telefone por um tempo muito longo. Eu costumava criar vários padrões de enrolamento para gerenciar a cobra adulta.

![](games4.jpg)

Temos o jogo de cobra no terminal Linux, graças ao nSnake . Use o comando abaixo para instalá-lo.
```
sudo apt-get install nsnake
```
Para jogar, digite o comando abaixo para iniciar o jogo.
```
nsnake
```
Use as setas para mover a cobra e alimentá-la.

**5. Greed**

É um pouco como Tron, menos a velocidade e a adrenalina.
Sua localização é indicada por um **'@'** piscando. Você está cercado por números e pode escolher se mover em qualquer uma das 4 direções,
A direção que você escolhe tem um número e você move exatamente esse número de etapas. E você repete o passo novamente. Você não pode revisitar o local visitado novamente e o jogo termina quando você não pode fazer um movimento.

Eu fiz parecer mais complicado do que realmente é.
Instale  com o comando abaixo:
```
sudo apt-get install greed
```
Para iniciar o jogo, use o comando abaixo. Em seguida, use as setas do teclado para jogar o jogo.
```
greed
```

**6. Controlador de Tráfego Aéreo**
O que é melhor do que ser piloto? Um controlador de tráfego aéreo. Você pode simular um sistema de tráfego aéreo inteiro no seu terminal. Para ser sincero, gerenciar o tráfego aéreo de um terminal é meio real.

![](games6.jpg)

Instale o jogo usando o comando abaixo:
```
sudo apt-get install bsdgames
```
Digite o comando abaixo para iniciar o jogo:
```
atc
```
ATC não é brincadeira de criança, é melhor ler o manual para aprender a jogar

**7. Tron**
Como essa lista pode ser completa sem um jogo de ação rápido?

![](games7.jpg)

Sim, o snap-in Tron está disponível no terminal Linux. Prepare-se para uma ação ágil e séria. Sem problemas de instalação nem problemas de instalação. Um comando irá iniciar o jogo. Tudo que você precisa é de uma conexão à Internet.
```
ssh sshtron.zachlatta.com
```
Você pode até jogar este jogo no modo multiplayer, se houver outros jogadores online. Leia mais sobre o jogo [Tron no Linux](https://itsfoss.com/play-tron-game-linux-terminal/) .

[Fonte itsfoss](https://itsfoss.com/best-command-line-games-linux/)