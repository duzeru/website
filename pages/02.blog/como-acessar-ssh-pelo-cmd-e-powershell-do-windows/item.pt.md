---
title: 'Como acessar SSH pelo CMD e PowerShell do Windows'
media_order: 'op01.jpg,op02.jpg,op03.jpg,op04.jpg,op05.jpg,op06.jpg,op07.jpg'
shortcode-citation:
    items: cited
    reorder_uncited: true
visible: true
feed:
    limit: 10
---

Este á uma alternativa bem interessante parar se conectar em um dispositivo via SSH sem sair do PowerShell ou do Prompt de Comando. Como você já deve saber há inúmeras formas de se fazer isso mas esta dica pode ser útil.

Projeto: [github.com/PowerShell/Win32-OpenSSH/releases](github.com/PowerShell/Win32-OpenSSH/releases)

Download: [https://github.com/PowerShell/Win32-OpenSSH/releases/download/v8.1.0.0p1-Beta/OpenSSH-Win64.zip](https://github.com/PowerShell/Win32-OpenSSH/releases/download/v8.1.0.0p1-Beta/OpenSSH-Win64.zip)

#### Passo 1
Realize o download do OpenSSH-Win64.zip e descompacte.
![](op01.jpg)

#### Passo 2

Abra o Windows PowerShell e entre no diretório OpenSSH-Win64 e execute o ssh.exe

```
PS C:\Users\Marcos> cd C:\Users\Marcos\Downloads\OpenSSH-Win64
PS C:\Users\Marcos\Downloads\OpenSSH-Win64>.\ssh.exe
```
![](op02.jpg)

#### Passo 3

Estabeleça a conexão com um host via SSH (ex: Host Linux)
```
PS C:\Users\Marcos\Downloads\OpenSSH-Win64>.\ssh.exe root@192.168.1.222
```
![](op03.jpg)

#### Passo 4
Confirme a conexão com yes em seguida digite a senha do usuário.
![](op04.jpg)

#### Passo 5
Conexão realizada com sucesso!
![](op05.jpg)

#### Passo 6
Para não ficar acessando o diretório do SSH você pode copiar os arquivos:
* ssh.exe
* libcrypto-41.dll

Para a pasta do **C:\Windows\System32** assim tanto via **PowerShell** ou **CMD** de Comando você pode utilizar o SSH.

Veja os exemplos:
**PowerShell**
![](op06.jpg)

**CMD**
![](op07.jpg)

Fonte: (https://www.100security.com.br/openssh-powershell-prompt-de-comando/)[https://www.100security.com.br/openssh-powershell-prompt-de-comando/]