---
title: Linux
media_order: linux.png
image_align: right
shortcode-citation:
    items: cited
    reorder_uncited: true
---

The GNU / Linux operating system is today an Operating System widely accepted in corporate environments, which has led to an increasing number of implementations in these environments, the main reasons that culminated in this strong adoption of Linux, includes its stable, secure and performance-oriented base for applications that support current and future businesses with a low total cost of ownership and customers' desire to avoid reliance on suppliers. Not to mention that it is widely linked to the creation of micro-services.

<a href="https://web.whatsapp.com/send?phone=5561994086468&text=Olá, como você poderia me ajudar com Docker?"><button class="btn btn-2 btn-sep fab fa-whatsapp">Let's chat on WhatsApp</button> </a>
<a href="https://t.me/claudioduzeru" target="_top"><button class="btn btn-1 btn-sep fab fa-telegram">Let's talk on Telegram</button></a>
<a href="mailto:claudiosilva@duzeru.org?Subject=Hello" target="_top"><button class="btn btn-4 btn-sep fas fa-envelope">Send an e-mail</button></a>