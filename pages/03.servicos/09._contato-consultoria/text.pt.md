---
title: contato-consultoria
image_align: left
shortcode-citation:
    items: cited
    reorder_uncited: true
---

<script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>

<table >
 <tr style="background-color:;text-align: left;vertical-align: top;">
    <td width="20%" >
      <div class="LI-profile-badge"  data-version="v1" data-size="medium" data-locale="pt_BR" data-type="vertical" data-theme="dark" data-vanity="claudio-duzeru"><a class="LI-simple-link" href='https://br.linkedin.com/in/claudio-duzeru?trk=profile-badge'>Claudio Antonio da Silva</a> </div>
     </td>
            
    <td width="29%" >
        Pergunte-me sobre nossos treinamentos ou consultoria em implantação ou configurações de Software Livre ou Open Source.

     </td>

<!-- ################ José #################### -->
    <td width="20%">
        <div class="LI-profile-badge"  data-version="v1" data-size="medium" data-locale="pt_BR" data-type="vertical" data-theme="dark" data-vanity="jose-paixao-barbosa-sousa-30626884"><a class="LI-simple-link" href='https://br.linkedin.com/in/jose-paixao-barbosa-sousa-30626884?trk=profile-badge'>Jose Paixao Barbosa Sousa</a></div>
    </td>
      
     <td width="29%">
         Entre em contato para contratar uma consultoria em gestão e boas práticas em ITIL, COBIT, ISO/IEC 20000.
      
</td>
  </tr>
</table>