---
title: 'CI CD'
media_order: ci-cd.png
image_align: right
shortcode-citation:
    items: cited
    reorder_uncited: true
class: small
features:
    -
        icon: 'fa fa-jsfiddle'
        header: Baixar
        text: Baixar
        url: opa
---

#### Integração e entrega contínua

Daremos a consultoria necessária para introduzir conceitos DevOps teóricos e práticos que traz a agilidade para Operacões e Desenvolvimento. A integração contínua (CI) dos vários códigos desenvolvidos, a garantia de que o código desenvolvido de forma ágil, pudessem chegar rapidamente em produção, através de uma entrega contínua (CD). A idéia é menos ações manuais, mais ações automáticas que vão do nascimento até a entrega do valor ao cliente.

<a href="https://web.whatsapp.com/send?phone=5561994086468&text=Olá, como você poderia me ajudar com Docker?"><button class="btn btn-2 btn-sep fab fa-whatsapp">Vamos conversar no WhatsApp?</button> </a>
<a href="https://t.me/claudioduzeru" target="_top"><button class="btn btn-1 btn-sep fab fa-telegram">Vamos conversar no Telegram?</button></a>
<a href="mailto:claudiosilva@duzeru.org?Subject=Hello" target="_top"><button class="btn btn-4 btn-sep fas fa-envelope">Envie um e-mail</button></a>