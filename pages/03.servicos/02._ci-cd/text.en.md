---
title: 'CI CD'
media_order: 'ci-cd.png,ci-cd.png'
image_align: right
shortcode-citation:
    items: cited
    reorder_uncited: true
class: small
features:
    -
        icon: 'fa fa-jsfiddle'
        header: Baixar
        text: Baixar
        url: opa
---

#### Integration and continuous delivery

We will provide the necessary consultancy to introduce theoretical and practical DevOps concepts that bring agility to Operations and Development. The continuous integration (CI) of the various codes developed, the guarantee that the code developed in an agile way, could arrive quickly in production, through a continuous delivery (CD). The idea is less manual actions, more automatic actions that go from birth to delivery of value to the customer.

<a href="https://web.whatsapp.com/send?phone=5561994086468&text=Olá, como você poderia me ajudar com Docker?"><button class="btn btn-2 btn-sep fab fa-whatsapp">Let's chat on WhatsApp</button> </a>
<a href="https://t.me/claudioduzeru" target="_top"><button class="btn btn-1 btn-sep fab fa-telegram">Let's talk on Telegram</button></a>
<a href="mailto:claudiosilva@duzeru.org?Subject=Hello" target="_top"><button class="btn btn-4 btn-sep fas fa-envelope">Send an e-mail</button></a>