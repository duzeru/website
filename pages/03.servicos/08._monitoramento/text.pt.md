---
title: Monitoramento
media_order: monitoring.png
image_align: right
shortcode-citation:
    items: cited
    reorder_uncited: true
---

Qualquer ambiente de TI - mesmo que tenha sido perfeitamente configurado pelo melhor analista da empresa e se encontre em pleno funcionamento - está sujeito a falhas e interrupções inesperadas, sejam elas por falhas física, lógicas ou humanas. Para minimizar o impacto, a indisponibilidade e ainda prever futuros incidentes, é de extrema importância que haja uma solução de monitoramento para o cenário em questão, pois este tem como objetivo reduzir o downtime do ambiente, sendo responsável por medir a disponibilidade e desempenho tanto dos componentes de infraestrutura, quanto das aplicações, gerando assim indicadores estratégicos para o negócio.

<a href="https://web.whatsapp.com/send?phone=5561994086468&text=Olá, como você poderia me ajudar com Monitoramento?"><button class="btn btn-2 btn-sep fab fa-whatsapp">Vamos conversar no WhatsApp</button> </a>
<a href="https://t.me/claudioduzeru" target="_top"><button class="btn btn-1 btn-sep fab fa-telegram">Vamos conversar no Telegram</button></a>
<a href="mailto:claudiosilva@duzeru.org?Subject=Hello" target="_top"><button class="btn btn-4 btn-sep fas fa-envelope">Envie um e-mail</button></a>