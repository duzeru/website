---
title: Monitoring
media_order: monitoring.png
image_align: right
shortcode-citation:
    items: cited
    reorder_uncited: true
---

Any IT environment - even if it has been perfectly configured by the best analyst in the company and is fully operational - is subject to unexpected failures and interruptions, whether due to physical, logical or human failures. To minimize the impact, unavailability and even predict future incidents, it is extremely important that there is a monitoring solution for the scenario in question, as it aims to reduce the downtime of the environment, being responsible for measuring the availability and performance of both infrastructure components and applications, thus generating strategic indicators for the business.

<a href="https://web.whatsapp.com/send?phone=5561994086468&text=Olá, como você poderia me ajudar com Docker?"><button class="btn btn-2 btn-sep fab fa-whatsapp">Let's chat on WhatsApp</button> </a>
<a href="https://t.me/claudioduzeru" target="_top"><button class="btn btn-1 btn-sep fab fa-telegram">Let's talk on Telegram</button></a>
<a href="mailto:claudiosilva@duzeru.org?Subject=Hello" target="_top"><button class="btn btn-4 btn-sep fas fa-envelope">Send an e-mail</button></a>