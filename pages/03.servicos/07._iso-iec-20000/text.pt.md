---
title: 'ISO IEC 20000'
media_order: iso20000.png
image_align: left
shortcode-citation:
    items: cited
    reorder_uncited: true
---

Empresas de todos os tamanhos dependem de uma gestão de serviços de TI eficiente. Não importa onde você esteja ou o que você faça, os seus serviços de TI precisam ser econômicos, confiáveis, consistentes e eficientes. É possível ter tudo isso com a ISO/IEC 20000, caso você gerencie serviços de TI internos ou forneça serviços de TI como um fornecedor de serviços terceirizado. Além disso, você fará com que a ITIL esteja em conformidade com as normas, de maneira que os seus serviços de TI proporcionem exatamente o que for necessário. 
**Você está pronto para a gestão de serviços de TI?

<a href="https://t.me/Jose_Paixao" target="_top"><button class="btn btn-1 btn-sep fab fa-telegram">Vamos conversar no Telegram?</button></a>
<a href="https://web.whatsapp.com/send?phone=5561993452469&text=Olá, como você poderia me ajudar com consultoria de melhores práticas e organizacional?"><button class="btn btn-2 btn-sep fab fa-whatsapp">Vamos conversar no WhatsApp?</button> </a>
<a href="mailto:josepaixao@duzeru.org?Subject=Olá" target="_top"><button class="btn btn-4 btn-sep fas fa-envelope">Envie um e-mail</button></a>