---
title: Docker
media_order: 'docker.png,docker.png'
image_align: left
shortcode-citation:
    items: cited
    reorder_uncited: true
---

Modernize your applications and facilitate migration to the cloud with Docker. Accelerate the delivery of new technologies, increase end-to-end security levels and reduce the cost of IT operation with the ideal platform for deploying DevOps. Deployment of applications quickly with Docker. A highly reliable and low-cost way to create, send and run distributed applications on any scale. Count on DuZeru's expertise to help you adopt a Swarm container conveyor.

<!--- Método dos botões coloridos -->
<style>
/* General button style */
.btn {
	border: none;
	font-family: 'Lato';
	font-size: inherit;
	color: inherit;
	background: none;
	cursor: pointer;
	padding: 25px 80px;
	display: inline-block;
	margin: 1px 3px;
	text-transform: uppercase;
	letter-spacing: 1px;
	font-weight: 700;
	outline: none;
	position: relative;
	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	transition: all 0.3s;
}

.btn:after {
	content: '';
	position: absolute;
	z-index: -1;
	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	transition: all 0.3s;
}

/* Pseudo elements for icons */
.btn:before {
	font-family: 'FontAwesome';
	speak: none;
	font-style: normal;
	font-weight: normal;
	font-variant: normal;
	text-transform: none;
	line-height: 1;
	position: relative;
	-webkit-font-smoothing: antialiased;
}


/* Icon separator */
/* TAMANHO DOS BOTOES*/
.btn-sep {
	padding: 8px 20px 8px 35px;
}

.btn-sep:before {
	background: rgba(0,0,0,0.15);
}

/* Button 1 */
.btn-1 {
	background: #3498db;
	color: #fff;
}

.btn-1:hover {
	background: #2980b9;
}

.btn-1:active {
	background: #2980b9;
	top: 2px;
}

.btn-1:before {
	position: absolute;
	height: 100%;
	left: 0;
	top: 0;
	line-height: 3;
	font-size: 80%;
	width: 20px;
}

/* Button 2 */
.btn-2 {
	background: #2ecc71;
	color: #fff;
}

.btn-2:hover {
	background: #27ae60;
}

.btn-2:active {
	background: #27ae60;
	top: 2px;
}

.btn-2:before {
	position: absolute;
	height: 100%;
	left: 0;
	top: 0;
	line-height: 3;
	font-size: 80%;
	width: 20px;
}

/* Button 3 */
.btn-3 {
	background: #e74c3c;
	color: #fff;
}

.btn-3:hover {
	background: #c0392b;
}

.btn-3:active {
	background: #c0392b;
	top: 2px;
}

.btn-3:before {
	position: absolute;
	height: 100%;
	left: 0;
	top: 0;
	line-height: 3;
	font-size: 80%;
	width: 20px;
}

/* Button 3 */
.btn-4 {
	background: #34495e;
	color: #fff;
}

.btn-4:hover {
	background: #2c3e50;
}

.btn-4:active {
	background: #2c3e50;
	top: 2px;
}

.btn-4:before {
	position: absolute;
	height: 100%;
	left: 0;
	top: 0;
	line-height: 3;
	font-size: 80%;
	width: 20px;
}

/* Icons */

.icon-cart:before {
	content: "\f07a";
}

.icon-heart:before {
	content: "\f55a";
}

.icon-info:before {
	content: "\f05a";
}

.icon-send:before {
	content: "\f1d8";
}
</style>
<!-- Inserir Botões aqui -->
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>

<a href="https://web.whatsapp.com/send?phone=5561994086468&text=Olá, como você poderia me ajudar com Docker?"><button class="btn btn-2 btn-sep fab fa-whatsapp">Let's chat on WhatsApp</button> </a>
<a href="https://t.me/claudioduzeru" target="_top"><button class="btn btn-1 btn-sep fab fa-telegram">Let's talk on Telegram</button></a>
<a href="mailto:claudiosilva@duzeru.org?Subject=Hello" target="_top"><button class="btn btn-4 btn-sep fas fa-envelope">Send an e-mail</button></a>