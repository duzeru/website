---
title: ITIL
media_order: 'itil.png,IMG_64_002.png'
image_align: left
shortcode-citation:
    items: cited
    reorder_uncited: true
---

ITIL busca promover a gestão com foco no cliente e na qualidade dos serviços de tecnologia da informação (TI). A ITIL lida com estruturas de processos para a gestão de uma organização de TI apresentando um conjunto abrangente de processos e procedimentos gerenciais, organizados em disciplinas, com os quais uma organização pode fazer sua gestão tática e operacional em vista de alcançar o alinhamento estratégico com os negócios. ITIL dá uma descrição detalhada sobre importantes práticas de IT com checklists, tarefas e procedimentos e instruções que uma organização de IT pode customizar para suas necessidades.

<a href="https://t.me/Jose_Paixao" target="_top"><button class="btn btn-1 btn-sep fab fa-telegram">Vamos conversar no Telegram?</button></a>
<a href="https://web.whatsapp.com/send?phone=5561993452469&text=Olá, como você poderia me ajudar com consultoria de melhores práticas e organizacional?"><button class="btn btn-2 btn-sep fab fa-whatsapp">Vamos conversar no WhatsApp?</button> </a>
<a href="mailto:josepaixao@duzeru.org?Subject=Olá" target="_top"><button class="btn btn-4 btn-sep fas fa-envelope">Envie um e-mail</button></a>