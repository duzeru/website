---
title: COBIT
media_order: cobit1.png
image_align: right
shortcode-citation:
    items: cited
    reorder_uncited: true
---

A gestão da informação é uma das ferramentas mais importantes para qualquer empresa porque é o que garante que as decisões corretas sejam tomadas. Com a tecnologia da informação isso não é diferente e ter governança de TI é fundamental para diminuir os riscos e conseguir resultados mais favoráveis e alinhados aos objetivos da empresa. Para garantir a governança, o COBIT é o principal conjunto de ferramentas.
O principal objetivo do COBIT é, naturalmente, a governança de TI, mas ele também serve para dar atenção ao foco do negócio, em vez de simplesmente se atentar aos serviços de TI como um todo.

<a href="https://t.me/Jose_Paixao" target="_top"><button class="btn btn-1 btn-sep fab fa-telegram">Vamos conversar no Telegram?</button></a>
<a href="https://web.whatsapp.com/send?phone=5561993452469&text=Olá, como você poderia me ajudar com consultoria de melhores práticas e organizacional?"><button class="btn btn-2 btn-sep fab fa-whatsapp">Vamos conversar no WhatsApp?</button> </a>
<a href="mailto:josepaixao@duzeru.org?Subject=Olá" target="_top"><button class="btn btn-4 btn-sep fas fa-envelope">Envie um e-mail</button></a>