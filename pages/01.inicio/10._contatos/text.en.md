---
title: Contact
image_align: left
shortcode-citation:
    items: cited
    reorder_uncited: true
---

## Contact

<style> 
.newspaper {
  column-count: 2;
  column-width: 100px;
}
</style>

<script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>

<div class="newspaper">
<div class="LI-profile-badge"  data-version="v1" data-size="medium" data-locale="pt_BR" data-type="vertical" data-theme="dark" data-vanity="claudio-duzeru"><a class="LI-simple-link" href='https://br.linkedin.com/in/claudio-duzeru?trk=profile-badge'>Claudio Antonio da Silva</a>

If you have suggestions, corrections or purchase support for the DuZeru operating system.
</br>
<a href="https://t.me/duzeru" target="_top"><button class="btn btn-1 btn-sep fab fa-telegram">Access the community on Telegram</button></a>

</br>
</br>
Ask me about our products, training or consulting, get in touch on any business day during business hours. I will be happy to assist you.
<a href="https://t.me/claudioduzeru" target="_top"><button class="btn btn-1 btn-sep fab fa-telegram">Shall we talk on Telegram?</button></a>
<a href="https://web.whatsapp.com/send?phone=5561994086468&text=Olá, como você poderia me ajudar com Docker?"><button class="btn btn-2 btn-sep fab fa-whatsapp">Shall we talk on WhatsApp?</button> </a>
    
<a href="mailto:claudiosilva@duzeru.org?Subject=Olá" target="_top"><button class="btn btn-4 btn-sep fas fa-envelope">Shall we talk on e-mail?</button></a>
</div>