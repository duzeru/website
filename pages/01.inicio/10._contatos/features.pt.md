---
title: Contatos
published: false
shortcode-citation:
    items: cited
    reorder_uncited: true
class: small
image_align: left
---

### Contato

<script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>

<table >
 <tr style="background-color:;text-align: left;vertical-align: top;">
    <td width="20%" >
      <div class="LI-profile-badge"  data-version="v1" data-size="medium" data-locale="pt_BR" data-type="vertical" data-theme="dark" data-vanity="claudio-duzeru"><a class="LI-simple-link" href='https://br.linkedin.com/in/claudio-duzeru?trk=profile-badge'>Claudio Antonio da Silva</a> </div>
     </td>
            
    <td width="29%" >
        Pergunte-me sobre nossos treinamentos ou consultoria em implantação ou configurações de Software Livre ou Open Source.
<a href="https://t.me/claudioduzeru" target="_top"><button class="btn btn-1 btn-sep fab fa-telegram">Vamos conversar no Telegram?</button></a>
<a href="https://web.whatsapp.com/send?phone=5561994086468&text=Olá, como você poderia me ajudar com consultoria em Operações?"><button class="btn btn-2 btn-sep fab fa-whatsapp">Vamos conversar no WhatsApp?</button> </a>
<a href="mailto:claudiosilva@duzeru.org?Subject=Olá" target="_top"><button class="btn btn-4 btn-sep fas fa-envelope">Vamos conversar pelo e-mail?</button></a>
     </td>
</tr>
</table>