---
title: Sobre
image_align: left
shortcode-citation:
    items: cited
    reorder_uncited: true
---

## Sobre

#### Nome, Logotipo, Codinome.

O **nome** já sugestivo, surgiu da junção de “do zero”, a princípio não foi planejado como um sistema que teria continuação, foi apenas pensado em ajudar usuários iniciantes em GNU/Linux principalmente com a versão T.I para estudantes que estão descobrindo o sistema GNU/Linux e terão dificuldades no inicio. O DuZeru 1 não teve apoio financeiro ou de colaboradores no desenvolvimento. Todos estes fatores ligavam para um nome que fosse algo de origem, primeiros passos, inicio, introdução, do zero. quando os caminhos se confundem é necessário voltar ao começo.

A **logo** tem a união do numero zero e do infinito. O infinito se divide duas metades, uma metade é o sistema e a outra é a comunidade, as duas se completam e formam um conhecimento infinito. o conhecimento começa do zero mas é infinito.

Os **codinomes** das versões são de animais ameaçados em extinção no Brasil.

* 1 Tucano
* 2 Arara Azul
* 2.2 Lobo Guará
* 3.0 Tatu-bola
* 4.0 Gato Maracajá

#### Missão
Proporcionar um sistema de fácil usabilidade, rápido e bonito. Unir pessoas na busca da construção e compartilhamento dos saberes adquiridos.

#### Visão
Ser referência junto às melhores distribuições GNU/Linux no mundo.

#### Desafios
Facilidade de uso, Este é a principal preocupação das distros. E esse desenvolvimento não é uma tarefa fácil de se realizar. as distribuições Brasileiras geralmente são desenvolvidas ou por uma pessoa ou um pequeno grupo, no entanto, uma coisa que muitos esquecem, é que essas pessoas precisam trabalhar e comer. È um desafio quando falamos em uma evolução ou estabilidade de uma distribuição, pois grande parte dos usuários nunca ouviu falar ou não dá atenção quando fala-­se em Software Livre, opensource, GPL ou GNU. Outro desafio é manter uma distribuição viva se não tem fins lucrativos, ou um apoio financeiro de entidades empresariais ou governamentais, Estes desafios podem ser superados se houver ao menos um bom planejamento e gestão de recursos financeiros e humanos. Deve-­se elaborar um projeto que seja atrativo e que estimule a interação dos usuários não apenas como usuários mais como colaboradores contribuintes de ideias para um sistema cada vez melhor, fazer com que o projeto seja sustentável financeiramente de alguma forma.

Alguns pontos que são primordiais no projeto da distribuição DuZeru:

* Cronograma (ciclo) de lançamento de uma nova versão;
* Instalador em modo gráfico objetivo;
* Priorizar leveza, beleza, e estabilidade do sistema;
* Visual gráfico agradável e organizado;
* Maior compatibilidade possível de hardware;
* Software para soluções diárias e codecs pré-instalados;

É notável que o projeto DuZeru tem mostrado excelentes propostas no desenvolvimento de soluções, envolvendo o GNU/Linux, tanto para um usuário comum, na área de ensino de Tecnologia ou na área de ensino fundamental com uma boa catalogação de softwares empacotados.

#### Objetivos
Este trabalho surgiu da observância da necessidade de dar continuidade ao sistema operacional versão 1, adaptando o Linux para as necessidades diárias e ter um sistema mais leve e moderno em virtude do hardware de muitas máquinas não suportar um sistema que requer um hardware mais atual. Além disso, observamos alguns aspectos como sustentabilidade e economia para o usuário, já que um sistema proprietário tem custo elevado. Outro aspecto é que usuários querem facilidade e agilidade, e concluímos que deve ser adaptado várias qualidades em um ambiente, estável e bem apresentável visualmente. O Sistema Operacional DuZeru, visa suprir estas necessidades, pois trará em suas configurações, uma interface gráfica intuitiva de fácil uso, com programas que proporcionarão os recursos complementares necessários para a implementação do computador nos processos diários.

#### Justificativa do Sistema Operacional 
A moeda de troca deste projeto voluntário é conhecimento mútuo, quando se faz sem interesses e com amor em primeiro lugar, fica bem feito. O que o sistema Operacional DuZeru faz é unir o melhor entre esses sistemas e interfaces em um só, apresentando-se como uma ótima distribuição para todos os tipos de usuários. excelente proveito e rapidez nas atividades, principalmente para novos usuários em sistemas GNU/Linux. A opção tecnológica por software livre também é estratégica para o desenvolvimento de soluções de governo, já que permite o avanço tecnológico de maneira segura e eficiente, sem o estabelecimento de dependência de fornecedores e consequente aprisionamento tecnológico. tecnológico e o combate à privatização do saber. Além disso, observamos alguns aspectos do software atualmente utilizado, não mais atualizado, e concluímos que seria mais fácil desenvolver um ambiente novo, mais bem apresentável visualmente. processos de aquisição do conhecimento assumem um papel de destaque exigindo pessoas críticas, criativas, reflexivas e com capacidade de pensar, de aprender a aprender, de trabalhar em grupo e de se conhecer como indivíduo. Além das facilidades e melhorias de desempenho, uma série de softwares pré-instalados, mantendo o usuário confortável no sistema operacional.

O universo GNU/Linux ainda desconhecido para alguns, é uma fonte de enorme conhecimento e possibilidades, basta apenas querer e saber explorar o seu potencial no mundo do código aberto.
“ Por “software livre” devemos entender aquele software que respeita a liberdade e senso de comunidade dos usuários. Grosso modo, os usuários possuem a liberdade de executar, copiar, distribuir, estudar, mudar e melhorar o software.Assim sendo, “software livre” é uma questão de liberdade, não de preço. Para entender o conceito, pense em “liberdade de expressão”, não em “cerveja  Grátis.”
(Fonte: http://www.gnu.org/philosophy/free-sw.pt-br.html).

Atualmente existem diversas e ótimas distribuições GNU/Linux, cada uma com suas particularidades e softwares instalados, a maioria delas são desenvolvidas por colaboradores. O grande diferencial de cada uma é sua filosofia e sua missão. Boas Ideias podem surgir das nossas próprias necessidades ou a partir  e problemas que tentamos solucionar, assim surgiu o DuZeru Linux para solucionar de maneira fácil questões de sustentabilidade, adaptação e melhor desempenho em desktops e notebooks. Une o melhor entre sistemas operacionais e interfaces Linux para atender ao máximo possível a necessidade do usuário casual, eventual, profissional ou estudante. A primeira versão foi baseado no Ubuntu 14.04 e foi compartilhado para download em 26 de Janeiro de 2015. Conquistando usuários, seu nome se espalhou redes sociais fazendo com que o sistema chegasse até no exterior como: Portugal, EUA, Itália, Chile dentre outros.