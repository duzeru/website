---
title: Home
content:
    items: '@self.modular'
shortcode-citation:
    items: cited
    reorder_uncited: true
body_classes: modular
---

