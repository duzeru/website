---
title: Apps
media_order: software.png
image_align: right
shortcode-citation:
    items: cited
    reorder_uncited: true
---

### Application development.

Here you will find several solutions for Desktop Linux. They were initially developed to meet the needs of DuZeru OS, but you can use them with ease in Debian-based distributions such as Ubuntu, Mint, PopOS and others.

<a href="https://gitlab.com/duzeru" target="_blank"><button class="btn btn-1 btn-sep icon-info">Click here, let's explore</button></a>
