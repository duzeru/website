---
title: Softwares
media_order: software.png
image_align: right
shortcode-citation:
    items: cited
    reorder_uncited: true
---

### Desenvolvimento de aplicativos

Aqui você encontrá diversas soluções para Desktop Linux. Foram desenvolvidas inicialmente para atender às necessidades do DuZeru OS, porém você poderá utilizar tranquilamente em distribuições baseadas em Debian como Ubuntu, Mint, PopOS e outras.

<a href="https://gitlab.com/duzeru" target="_blank"><button class="btn btn-1 btn-sep icon-info">Clique aqui, vamos explorar</button></a>
