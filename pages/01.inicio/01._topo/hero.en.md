---
title: Top
media_order: hero.jpg
shortcode-citation:
    items: cited
    reorder_uncited: true
body_classes: 'header-dark header-transparent'
hero_classes: 'text-light title-h1h2 overlay-dark-gradient hero-large parallax'
hero_image: hero.jpg
---

## Welcome to **DuZeru**
#### Technological solutions based on the world of Free Software and Open Source.