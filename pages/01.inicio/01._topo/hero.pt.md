---
title: topo
media_order: hero.jpg
shortcode-citation:
    items: cited
    reorder_uncited: true
body_classes: 'header-dark header-transparent'
hero_classes: 'text-light title-h1h2 overlay-dark-gradient hero-large parallax'
hero_image: hero.jpg
---

## Seja bem-vindo ao **DuZeru**
#### Soluções tecnológicas baseadas no mundo do Software Livre e Open Source.