---
title: 'Oferta de treinamento'
image_align: left
shortcode-citation:
    items: cited
    reorder_uncited: true
---

### Nota importante.

Prezados, anteriormente eu havia citado que esta versão distribuída de imagem ISO do DuZeru poderia ser a ultima. **Realmente aconteceu**.
Apesar de ter feito um sucesso notável Nacional e internacionalmente, **o projeto acabou fracassando em atrair um grupo de desenvolvedores interessados em participar de forma ativa do desenvolvimento**. Este e outros fatores (entre eles a justificada cobrança em torno de novos recursos e melhorias) acabaram fazendo com que o projeto seja descontinuado.
**Resumidamente, a cobrança e a demanda tornou-se maior que a capacidade de entrega**. Por favor, não critique. Todos os esforços de desenvolvimento, design, custos financeiros por entre os anos foi colaborativo, inevitavelmente todo projeto têm começo, meio e fim.
Minha vida profissional, pessoal e familiar mudou muito desde os primórdios do projeto, e hoje não consigo mais o foco de gestão ao projeto do sistema operacional. É melhor fechar com chave de ouro com uma versão com tanta qualidade gráfica quanto de aplicativos desenvolvidos internamente do que degradar a qualidade na entrega.

Fica aqui os agradecimentos à todos que diretamente ou indiretamente participaram e aprenderam junto ao projeto do Sistema Operacional.
Aqui está todos os códigos da imagem ISO e aplicativos desenvolvidos por mim e colaboradores.
https://gitlab.com/duzeru

Meu contato https://t.me/claudioduzeru

O projeto DuZeru não deixará de existir, apenas o Sistema Operacional. Será dado continuidade em atividades com treinamentos e consultoria de Software Livre e Open Source.