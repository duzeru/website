---
title: Blog
media_order: blog1.png
image_align: left
shortcode-citation:
    items: cited
    reorder_uncited: true
---

### Blog
We are always documenting, translating and creating and publishing articles and posts linked to the technological world of Free Open Source Software. See the tips we have for you.


<a href="https://duzeru.org/blog"><button class="btn btn-3 btn-sep icon-heart">Click here, let's explore the Blog </button></a>