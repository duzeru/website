---
title: Blog
media_order: blog1.png
image_align: left
shortcode-citation:
    items: cited
    reorder_uncited: true
---

### Blog
Sempre estamos documentando, traduzindo e criando e publicando artigos e postagens vinculadas ao mundo tecnológico do Software Livre Open Source. Veja as dicas que temos para você.


<a href="https://duzeru.org/blog"><button class="btn btn-3 btn-sep icon-heart"> Clique aqui, vamos eplorar o Blog </button></a>