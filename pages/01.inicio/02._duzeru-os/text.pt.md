---
title: 'DuZeru OS'
media_order: duzeru-v4.1.torrent
image_align: left
shortcode-citation:
    items: cited
    reorder_uncited: true
body_classes: overlay-dark-gradient
---

#### DuZeru OS 4.1
<script id="chatBroEmbedCode">/* Chatbro Widget Embed Code Start */function ChatbroLoader(chats,async){async=!1!==async;var params={embedChatsParameters:chats instanceof Array?chats:[chats],lang:navigator.language||navigator.userLanguage,needLoadCode:'undefined'==typeof Chatbro,embedParamsVersion:localStorage.embedParamsVersion,chatbroScriptVersion:localStorage.chatbroScriptVersion},xhr=new XMLHttpRequest;xhr.withCredentials=!0,xhr.onload=function(){eval(xhr.responseText)},xhr.onerror=function(){console.error('Chatbro loading error')},xhr.open('GET','//www.chatbro.com/embed.js?'+btoa(unescape(encodeURIComponent(JSON.stringify(params)))),async),xhr.send()}/* Chatbro Widget Embed Code End */ChatbroLoader({encodedChatId: '22Qzj'});</script>

<div style="position: absolute; left: 52%;  top: 20%;" >
    <iframe width="560" height="315" src="https://www.youtube.com/embed/wpSuSWcO95Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

[size=13]
|   Baixo consumo de Hardware | e beleza unidos com:   |
|  :-----   |  :-----  |
| **CPU**: | 1.0 Ghz |          | **CODENAME**: | Maracajá |
|**RAM**: | 512MB |            | **KERNEL**: | 4.19 |
| **HD**:  | 4.5Gb |               | **DESKTOP**: | Xfce |
| **VÍDEO**: | 12Mb |          | **ISO**: | 1.3GB |
| **VGA**: | 640 x 480 |      | **LIVE**: | Sim | 
|  **Aquitetura** |  x64 EFI MBR/BIOS Legacy | | **INSTALADOR**: | Calamares
| **BASE**: |Debian Stable | |
[/size]

<!--- Método dos botões coloridos -->
<style>
/* General button style */
.btn {
	border: none;
	font-family: 'Lato';
	font-size: inherit;
	color: inherit;
	background: none;
	cursor: pointer;
	padding: 25px 80px;
	display: inline-block;
	margin: 1px 3px;
	text-transform: uppercase;
	letter-spacing: 1px;
	font-weight: 700;
	outline: none;
	position: relative;
	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	transition: all 0.3s;
}

.btn:after {
	content: '';
	position: absolute;
	z-index: -1;
	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	transition: all 0.3s;
}

/* Pseudo elements for icons */
.btn:before {
	font-family: 'FontAwesome';
	speak: none;
	font-style: normal;
	font-weight: normal;
	font-variant: normal;
	text-transform: none;
	line-height: 1;
	position: relative;
	-webkit-font-smoothing: antialiased;
}


/* Icon separator */
/* TAMANHO DOS BOTOES*/
.btn-sep {
	padding: 8px 20px 8px 35px;
}

.btn-sep:before {
	background: rgba(0,0,0,0.15);
}

/* Button 1 */
.btn-1 {
	background: #3498db;
	color: #fff;
}

.btn-1:hover {
	background: #2980b9;
}

.btn-1:active {
	background: #2980b9;
	top: 2px;
}

.btn-1:before {
	position: absolute;
	height: 100%;
	left: 0;
	top: 0;
	line-height: 3;
	font-size: 80%;
	width: 20px;
}

/* Button 2 */
.btn-2 {
	background: #2ecc71;
	color: #fff;
}

.btn-2:hover {
	background: #27ae60;
}

.btn-2:active {
	background: #27ae60;
	top: 2px;
}

.btn-2:before {
	position: absolute;
	height: 100%;
	left: 0;
	top: 0;
	line-height: 3;
	font-size: 80%;
	width: 20px;
}

/* Button 3 */
.btn-3 {
	background: #e74c3c;
	color: #fff;
}

.btn-3:hover {
	background: #c0392b;
}

.btn-3:active {
	background: #c0392b;
	top: 2px;
}

.btn-3:before {
	position: absolute;
	height: 100%;
	left: 0;
	top: 0;
	line-height: 3;
	font-size: 80%;
	width: 20px;
}

/* Button 3 */
.btn-4 {
	background: #34495e;
	color: #fff;
}

.btn-4:hover {
	background: #2c3e50;
}

.btn-4:active {
	background: #2c3e50;
	top: 2px;
}

.btn-4:before {
	position: absolute;
	height: 100%;
	left: 0;
	top: 0;
	line-height: 3;
	font-size: 80%;
	width: 20px;
}

/* Icons */

.icon-cart:before {
	content: "\f07a";
}

.icon-heart:before {
	content: "\f55a";
}

.icon-info:before {
	content: "\f05a";
}

.icon-send:before {
	content: "\f1d8";
}
</style>
<!-- Inserir Botões aqui -->
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>

<a href="https://distrowatch.com/duzeru" onclick="window.open('https://sourceforge.net/projects/duzeru/files/DuZeru_4/dz4.1-amd64.efi-mbr.iso','newwin');"><button class="btn btn-1 btn-sep fa fa-jsfiddle">Baixar</button></a>
<a href="https://distrowatch.com/duzeru" onclick="window.open('https://sourceforge.net/projects/duzeru/files/DuZeru_4/duzeru-v4.1.torrent','newwin');"><button class="btn btn-1 btn-sep fa fa-jsfiddle">Torrent</button></a>


<a href="https://sourceforge.net/projects/duzeru/files/DuZeru_4/dz4.1-amd64.efi-mbr.md5" target="_blank"><button class="btn btn-3 btn-sep fa fa-file-code-o">MD5</button></a>
<a href="https://distrotest.net/DuZeru" target="_blank"><button class="btn btn-4 btn-sep fa fa-desktop">Test Online</button></a>
<a href="https://distrowatch.com/duzeru" target="_blank"><button class="btn btn-4 btn-sep fa fa-linux">DistroWatch</button>

    

<a href="https://distrowatch.com/duzeru" onclick="window.open('https://t.me/duzeru','newwin');"><button class="btn btn-1 btn-sep fa fa-telegram">Comunidade Telegram</button></a>
[![Download DuZeru GNU/Linux](https://img.shields.io/sourceforge/dt/duzeru.svg)](https://sourceforge.net/projects/duzeru/files/latest/download)

<!--
<button class="btn btn-1 btn-sep fa fa-info">Azul</button>

<button class="btn btn-2 btn-sep icon-cart">Verde</button>

<button class="btn btn-3 btn-sep fa fa-heart">Vermelho</button>

<button class="btn btn-4 btn-sep icon-send">Cinza</button>
-->
