---
title: Consult
media_order: consultoria.png
image_align: left
shortcode-citation:
    items: cited
    reorder_uncited: true
---

#### Consult us.
* Implementation of micro-services in Docker and Swarm containers;
* Knowledge base, website creation;
* Automation integration, delivery and continuous software deployment;
* Best practices DevOps and ITIL;

All tools are Open Source and Free Software, there are no license costs.
We have the perfect solution for your need.

<a href="https://duzeru.org/consultoria"><button class="btn btn-1 btn-sep icon-info">Click here, let's explore the possibilities.</button> </a>