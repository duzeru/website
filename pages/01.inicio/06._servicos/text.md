---
title: Consultoria
media_order: consultoria.png
image_align: left
shortcode-citation:
    items: cited
    reorder_uncited: true
---

#### Faça uma consultoria
* Implantação de micro-serviços em container Docker e Swarm;
* Base de conhecimento, criação de websites;
*  Automação integração, entrega e implantação contínua de software;
* Melhores práticas DevOps e ITIL;

Todas as ferramentas são Open Source e Software Livre, não há custos de licença.
Temos a solução perfeita para sua necessidade.

<a href="https://duzeru.org/consultoria"><button class="btn btn-1 btn-sep icon-info">Clique aqui, vamos explorar as possibilidades</button> </a>