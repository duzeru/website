---
title: Servicos
media_order: consultoria.png
image_align: left
shortcode-citation:
    items: cited
    reorder_uncited: true
---

#### Solicite uma orientação para sua empresa ou negócio em:
* Implantação de micro-serviços em container Docker e Swarm;
* Base de conhecimento, criação de websites;
*  Automação integração, entrega e implantação contínua de software;
* Melhores práticas DevOps, ITIL, BPMN, COBIT e ISO 20000;

Todas as ferramentas são Open Source e Software Livre, não há custos de licença.
Temos a solução perfeita para sua necessidade.

<a href="https://duzeru.org/servicos"><button class="btn btn-1 btn-sep icon-info">Clique aqui, vamos explorar as possibilidades</button> </a>